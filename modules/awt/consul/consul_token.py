#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# ToDo - policy suboptions name/id are mutually exclusive. one or the other. Needs enforcement.
# ToDo - policy suboptions name/id are mutually exclusive. one or the other. Needs enforcement.
# ToDo - Generate token using basic and complex config to determine what Consul returns.

ANSIBLE_METADATA = {'metadata_version': '1.1', 'status': ['preview'], 'supported_by': 'awt'}

DOCUMENTATION = r'''
---
module: consul_token

short_description: modify Hashicorp Consul ACL Policies

version_added: "2.9"

description:
    - C(consul_token) is used to create and maintain Consul ACL tokens.

options:
    accessor_id:
        default: None
        description: 
        	- a UUID to use as the token's accessor id. Will be generated if not provided.
        	- This an immutable field and is not considered part of the Token's configuration.
        required: false
        type: str
    auth_token:
      description
        - a Consul token with create ACL read, write, and delete privileges.
    description:
        default: ''
        description: 
            - Free form human readable description of the token
        type: str
    expiration_time:
        default: ""
        description:
            - If set this represents the point after which a token should be considered revoked and is eligible
             for destruction. The default unset value represents NO expiration. This value must be between 1 minute 
             and 24 hours in the future. Added in Consul 1.5.0
        	- This an immutable field and is not considered part of the Token's configuration.
        required: false
        type: str
    expiration_ttl:
        default: ""
        description:
            - This is a convenience field and if set will initialize the ExpirationTime field to a 
            value of CreateTime + ExpirationTTL. This field is not persisted beyond its initial use. Can 
            be specified in the form of "60s" or "5m" (i.e., 60 seconds or 5 minutes, respectively). This 
            value must be no smaller than 1 minute and no longer than 24 hours. Added in Consul 1.5.0.
        	- This an immutable field and is not considered part of the Token's configuration.
        required: false
        type: str    
    host:
        default: 127.0.0.1
        description: 
            - The address of the Consul host responsible for servicing the request.
        required: false
        type: str
    local:
        default: false
        description: 
            - if true, indicates that the token should not be replicated globally
        required: false
    namespace:
        description: 
            - Specifies the namespace to create the role. If not provided in the JSON body, 
            the value of the ns URL query parameter or in the X-Consul-Namespace header will be used. 
            If not provided at all, the namespace will be inherited from the request's ACL token or will 
            default to the default namespace. Added in Consul 1.7.0
        required: false
        type: str
    policies:
        description:
            - The list of policies that should be applied to the role. A PolicyLink is an object with 
                an "ID" and/or "Name" field to specify a policy. With the PolicyLink, roles can be linked 
                to policies either by the policy name or by the policy ID. When policies are linked by name 
                they will be internally resolved to the policy ID. With linking roles internally by IDs, 
                Consul enables policy renaming without breaking tokens.
        type: list
        elements: dict
        required: false
        suboptions:
            id:
                description: the name identifying the policy
                required: false
                type: str
            name:
                description: the name identifying the policy
                required: false
                type: str
        secret_id:
        default: None
        description:
            - a UUID to use as the token's accessor id. Will be generated if not provided.
        required: false
        type: str
    port:
        default: 8500
        description: 
            - The port the Consul host responsible for servicing the request is listening on.
        required: false
        type: int
    roles:
        description:
            - The list of roles that should be applied to the token. A RoleLink is an object with an "ID" 
            and/or "Name" field to specify a role. With the RoleLink, tokens can be linked to roles either 
            by the role name or by the role ID. When roles are linked by name they will be internally resolved 
            to the role ID. With linking tokens internally by IDs, Consul enables role renaming without breaking 
            tokens. Added in Consul 1.5.0.
        elements: dict
        type: list
        elements: dict
        required: false
        suboptions:
            name:
                description: the name of the role.
                default: None
                required: false
                type: str
            id:
                description: the id of the role
                default: None
                required: false
                type: str    
    scheme:
        default: http
        description: 
            - The HTTP scheme to use to connect to the Consul server.
        required: false
        type: int
    service_identities:
        description: 
            - The list of service identities that should be applied to the role. Added in Consul 1.5.0.
        required: false
        type: list
        elements: dict
        suboptions:
            datacenters:
                default: []
                description: A list of datacenters in which the role policies will be in effect. If not
                specified defaults to all datacenters
                required: false
                type: list
                elements: str
            name:
                description: The name of the service. The name must be no longer than 256 characters, must start and
                    end with a lowercase alphanumeric character, and can only contain lowercase alphanumeric characters
                     as well as - and _.
                required: true
                type: str
    state:
        description:
            - Whether the token should be present or absent
        type: str
        choices: [ present, absent ]
        default: [ present ]


author:
    - Stephen Nesbitt <steve.nesbitt@absaroka-tech.com>
'''

EXAMPLES = '''
# Create a Consul Token
- name: Add Consul Token1
  consul_token:
    auth_token: 'this_uuid'
    host: 'this_url'
    port: 123
    scheme: 'https'
  	accessor_id: 88b4309f-e7ea-4b3f-b28e-abf839ad4ac8
    description: A test token
    name: test_token
    policies:
        - ID: 717aed3d-898d-4357-913e-3d0a861cc453
        - Name: test-policy        
    roles:
        - ID: 717aed3d-898d-4357-913e-3d0a861cc45d
        - Name: test-role1
    secret_id: 228fd6b1-5d1e-4229-bf01-c4622e1e2da4
    state: present

- name: Add Consul Token
  consul_token:
  	accessor_id: 88b4309f-e7ea-4b3f-b28e-abf839ad4ac8
    description: A test token
    local: true
    name: test_token
    policies: [{ "Name": "test-token"}]
    roles: [{'Name': 'test-role'}]
    secret_id: 228fd6b1-5d1e-4229-bf01-c4622e1e2da4
    state: present
'''

RETURN = '''
original_message:
    description: The original name param that was passed in
    type: str
    returned: always
message:
    description: The output message that the test module generates
    type: str
    returned: always
'''

from ansible.module_utils.awt.base import Builder, Director, Config, ModuleSvc, ModuleRunner
from ansible.module_utils.awt.consul import CONSUL_CONNECTION_ARGUMENT_SPEC, ConsulSvc, ConsulConfigurable
from ansible.module_utils.basic import AnsibleModule
from consul import Consul
from enum import Enum
import uuid


class TokenConfig(Config):
    BaseMap = Enum('RoleMap', [('description', 'Description'),
                               ('local', 'Local'),
                               ('namespace', 'Namespace'),
                               ('node_identities', 'NodeIdentities'),
                               ('policies', 'Policies'),
                               ('roles', 'Roles'),
                               ('secret_id', 'SecretID'),
                               ('service_identities', 'ServiceIdentities')])

    NodeIdentityMap = Enum('NodeIdentityMap',
                           [('name', 'NodeName'),
                            ('datacenter', 'Datacenter')]
                           )

    PolicyMap = Enum('BaseMap',
                     [('name', 'Name'),
                      ('id', 'ID')]
                     )

    RoleMap = Enum('RoleMap', [
        ('id', 'ID'),
        ('name', 'Name')])

    ServiceIdentityMap = Enum('ServiceIdentityMap',
                              [('name', 'ServiceName'),
                               ('datacenters', 'Datacenters')])

    @classmethod
    def factory_config_from_module_params(cls, module_params: dict) -> Config:
        config_dict = module_params  # We don't want to modify module_params, so we create a copy
        NODEID_KEY = "node_identities"
        POLICY_KEY = "policies"
        ROLES_KEY = 'roles'
        SERVICEID_KEY = "service_identities"

        def map_list(items: list, map: Enum) -> list:
            mapped_list = []
            for node in items:
                # ToDo - need unit test to verify that use of a key the Enum does not have defined does nothing rather
                # than throw an error
                translated = {map[key].value: value for (key, value) in node.items() if key in map.__members__ and
                              map[key].value is not None}
                mapped_list.append(translated)
            return mapped_list

        if module_params.get(NODEID_KEY) is not None:
            config_dict[NODEID_KEY] = map_list(module_params[NODEID_KEY], TokenConfig.NodeIdentityMap)

        if module_params.get(POLICY_KEY) is not None:
            config_dict[POLICY_KEY] = map_list(module_params[POLICY_KEY], TokenConfig.PolicyMap)

        if module_params.get(ROLES_KEY) is not None:
            config_dict[ROLES_KEY] = map_list(module_params[ROLES_KEY],
                                              TokenConfig.RoleMap)

        if module_params.get(SERVICEID_KEY) is not None:
            config_dict[SERVICEID_KEY] = map_list(module_params[SERVICEID_KEY],
                                                  TokenConfig.ServiceIdentityMap)

        # Not all module_params are configuration settings so do not process those which are not a member of the token
        # map
        config_dict = {TokenConfig.BaseMap[k].value: v for k, v in module_params.items()
                       if v is not None and k in TokenConfig.BaseMap.__members__}
        return TokenConfig(config_dict)

    @classmethod
    def factory_config_from_query(cls, query_result: dict) -> Config:
        # The Consul API query for a token includes a list of policy dictionaries.
        # One of the dictionary keys is ID.
        # Since the API does not let us set the key, we have no way to know what it is
        # and its presence will cause the requested vs existing configuration check to fail.
        # Therefore we strip it off.
        if not query_result:
            return TokenConfig({})
        else:
            #ToDo Unit test for policies = None (query returning with out any policies
            #ToDo Unit test for policies = None (query returning with out any policies
            #ToDo List comprehension
            normalized_policies = []
            policies = query_result.get('Policies', None)
            if policies is not None:
                for policy in policies:
                    # raise Exception(f"qr: {query_result}\n{query_result.get('Policies')}")
                    policy.pop('ID', None)
                    normalized_policies.append(policy)

            normalized_roles = []
            roles = query_result.get('Roles', None)
            if roles is not None:
                for role in roles:
                    role.pop('ID')
                normalized_roles.append(roles)
                # raise Exception(f"Policies {normalized_policies}")
                # raise Exception(f"qr: {query_result}")
            config_dict = {'AuthID': query_result.get('AuthId'),
                               'Description': query_result.get('Description'),
                               'ExpirationTime': query_result.get('ExpirationTime'),
                               'ExpirationTTL': query_result.get('ExpirationTTL'),
                               'Local': query_result.get('Local'),
                               'Namespace': query_result.get('Namespace'),
                               'NodeIdentities': query_result.get('NodeIdentities'),
                               'Policies': query_result.get('Policies'),
                               'Roles': query_result.get('Roles'),
                               'ServiceIdentities': query_result.get('ServiceIdentities')}

            # Strip any keys whose value is None
            config_dict = {k: v for k, v in config_dict.items() if v is not None}
            return TokenConfig(config_dict)

    def __init__(self, signature: dict):
        super().__init__(signature)


class TokenSvc(ConsulSvc):
    @property
    def _endpoint(self):
        return self._client.acl.tokens

    def __init__(self, client):
        super().__init__(client)

    def create(self, payload: dict) -> dict:
        return self._endpoint.create(payload)

    def list(self) -> list:
        return self._endpoint.list()

    def update(self, id, payload):
        return self._endpoint.update(payload, id)


class Token(ConsulConfigurable):
    def __init__(self):
        super().__init__()

    # FixMe - Obsolete
    def generate_id(self, module_params: dict) -> object:
        # return module_params['accessor_id']
        return None

    def delete(self):
        return self._svc.delete(self.id)

    def create(self):
        payload = self._requested_config.signature
        payload['AccessorID'] = self.id
        return self._svc.create(payload)

    def update(self):
        return self._svc.update(self.id, self._requested_config.signature)

    def get_requested_config(self, module_params) -> Config:
        config = TokenConfig.factory_config_from_module_params(module_params)
        return config

    def get_existing_config(self) -> TokenConfig:
        query_result = self._svc(self.id)
        return TokenConfig.factory_config_from_query(query_result)

    def __repr__(self):
        return f"ConsulToken Configurable.\n" + \
               f"changes pending {self.changes_pending}\n" + \
               f"display name: {self.display_name}\n" + \
               f"id: {self.id}\n" + \
               f"exists: {self.exists}\n" + \
               f"existing_config: {self._existing_config}\n" + \
               f"requested_config: {self._requested_config}"


class TokenBuilder(Builder):
    def __init__(self, module_: AnsibleModule, svc_: ModuleSvc) -> None:
        self.module_ = module_
        self._svc = svc_
        self.reset()

    def reset(self) -> None:
        self._configurable = Token()

    @property
    def configurable(self) -> Token:
        configurable = self._configurable
        self.reset()
        return configurable

    def produce_service(self) -> None:
        self._configurable._svc = self._svc

    def produce_id(self) -> None:
        self._configurable.id_ = self.module_.params.pop('accessor_id')

    def produce_requested_config(self) -> None:
        self._configurable._requested_config = self._configurable.get_requested_config(self.module_.params)

    def process_existing(self) -> None:
        # raise Exception(f"existing: {self._configurable.id}")
        existing = self._configurable.svc.get(self._configurable.id)

        self._configurable._existing_config = TokenConfig.factory_config_from_query(existing)


ROLE_ARGUMENT_SPEC = dict(accessor_id=dict(required=True, type='str'),
                          description=dict(default='', type='str', required=False),
                          # if expiration_time is not set, Consul API does not return the field.
                          # Ergo, the default is None which will tell the mapper not to include it in such a case.
                          expiration_time=dict(default=None, required=False, type="str"),
                          expiration_ttl=dict(default='0s', required=False, type="str"),
                          local=dict(default=False, required=False, type='bool'),
                          namespace=dict(type='str', required=False),
                          # NodeIdentities is commented out because in Consul 1.8.0
                          # including that object results in an unknown field error
                          # node_identities=dict(type='list', elements='dict', required=False),
                          policies=dict(type='list', elements='dict', required=False),
                          roles=dict(type='list', elements='dict', required=False),
                          secret_id=dict(type="str", required=False, no_log=True),
                          service_identities=dict(type='list', elements='dict', required=False),
                          state=dict(type="str", choices=['present', 'absent'], default='present'))

# Combine Consul connection parameters with role specific parameters)
ARGUMENT_SPEC = {**CONSUL_CONNECTION_ARGUMENT_SPEC, **ROLE_ARGUMENT_SPEC}


def init_svc(module_params):
    client = Consul(host=module_params.pop('host'),
                    token=module_params.pop('auth_token'),
                    port=module_params.pop('port'),
                    scheme=module_params.pop('scheme'))
    return TokenSvc(client)


if __name__ == '__main__':
    module = AnsibleModule(ARGUMENT_SPEC)
    svc = init_svc(module.params)
    director = Director()
    builder = TokenBuilder(module, svc)
    director.builder = builder
    director.build()

    # module.check_mode = True
    result = ModuleRunner(builder.configurable, module).run()
    module.exit_json(changed=result.changed,
                     meta=result.meta,
                     msg="{}".format(result.msg))
