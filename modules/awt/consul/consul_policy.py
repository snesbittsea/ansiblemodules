#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright: (c) 20120, Stephen V Nesbitt <steve.nesbitt@absaroka-tech.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

DOCUMENTATION = r'''
---
module: consul_policy
short_description: Manage Hashicorp Consul ACL Policies
description:
    - C(consul_policy) is used to create and maintain Consul ACL policies.
version_added: "2.9"
author: "Stephen Nesbitt (@absaroka-tech.com)
options:
    auth_token:
      description:
        - a Consul token with create ACL read, write, and delete privileges.
        required: true
	datacenters:
		description:
			- Specifies the datacenters the policy is valid within. When no datacenters are provided the policy 
			is valid in all datacenters including those which do not yet exist but may in the future.
		required: false
		type: list
    description:
        default: ''
        description: A description of the policy
        required: false
        type: str
    host:
        default: 127.0.0.1
        description: 
            - The address of the Consul host responsible for servicing the request.
        required: false
        type: str
    name:
        description:
            - Specifies a name for the ACL policy. The name can contain alphanumeric characters, 
            dashes -, and underscores _. This name must be unique.
        type: str
        required: true
	namespace:
		default: None
		description:
			- Specifies the namespace to create the policy. If not provided, the namespace 
			will be inherited from the request's ACL token or will default to the default namespace.
		required: false
		type: str
    port:
        default: 8500
        description: 
            - The port the Consul host responsible for servicing the request is listening on.
        required: false
        type: int
    rules:
        description:
            -  Specifies rules for the ACL policy.
        required: true
        type: str
    scheme:
        default: http
        description: 
            - The HTTP scheme to use to connect to the Consul server.
        required: false
        type: int
    state:
        description:
            - Whether the policy should be present or absent
        type: str
        choices: [ present, absent ]
        default: [ present ]
    token:
      description
        - a Consul token with create ACL policy privileges.
requirements:
    - python-consul2 (https://github.com/poppyred/python-consul2/blob/master/consul/base.py)
seealso:
    - name: Hashicorp ACL Policy API
      description: Official documentation for the Hashicorp Consul Policy API
      link: https://www.consul.io/api-docs/acl/policies
'''

EXAMPLES = '''
# Create a Consul Policy
- name: Add Consul Policy
  consul_policy:
    scheme:
        default: http
        description: 
            - The HTTP scheme to use to connect to the Consul server.
        required: false
        type: int
    name: ui-read
    description: Allows read access to the Consul UI
    rules: 'key "" { policy = "read" }'
    state: present
'''

RETURN = r'''
changed:
    description: Whether the module configuration was changed.
    returned: success
    type: bool
meta:
    description: Meta data releant to the module's operation.
    returned: success
    type: str
msg:
    description: Summary of what the module did.
    returned: success
    type: str

'''

from ansible.module_utils.awt.base import Builder, Director, Config, ModuleSvc, ModuleRunner
from ansible.module_utils.awt.consul import CONSUL_CONNECTION_ARGUMENT_SPEC, ConsulSvc, ConsulConfigurable
from ansible.module_utils.basic import AnsibleModule
from enum import Enum
import consul


# ToDo - cleanup documentation
# ToDo - consider adding return constructor
# ToDo - doc fragment for connection parameters since shared
# ToDo - Analyze Datacenters. Should it return [] or None if not existent?

class PolicyConfig(Config):
    """Encapsulates a Consul Policy configuration.

    """
    BaseMap = Enum('BaseMap', [('description', 'Description'),
                               ('datacenters', 'Datacenters'),
                               ('namespace', 'Namespace'),
                               ('rules', 'Rules')])

    @classmethod
    def factory_config_from_module_params(cls, module_params: dict) -> Config:
        config_dict = module_params  # We don't want to modify module_params, so we create a copy

        # Not all module_params are configuration settings so do not process those which are not a member of the policy
        # map
        config_dict = {PolicyConfig.BaseMap[k].value: v for k, v in module_params.items()
                       if v is not None and k in PolicyConfig.BaseMap.__members__}
        return PolicyConfig(config_dict)

    @classmethod
    def factory_config_from_query(cls, query_result: dict) -> Config:
        config_dict = {'Description': query_result.get('Description'),
                       'Datacenters': query_result.get('Datacenters'),
                       'Namespace': query_result.get('Namespace'),
                       'Rules': query_result.get('Rules')}

        config_dict = {k: v for k, v in config_dict.items() if v is not None}
        return PolicyConfig(config_dict)

    def __init__(self, signature: dict):
        super().__init__(signature)


class PolicySvc(ConsulSvc):
    @property
    def _endpoint(self):
        return self._client.acl.policy

    def __init__(self, client):
        super().__init__(client)

    def create(self, name, datacenters=None, description=None, rules=None) -> dict:
        return self._endpoint.create(name, description, rules, datacenters)

    def list(self) -> list:
        return self._endpoint.list()

    def update(self, consul_id, name, datacenters=None, description=None, rules=None):
        return self._endpoint.update(consul_id,
                                     name,
                                     description,
                                     rules,
                                     datacenters)


class Policy(ConsulConfigurable):
    def __init__(self):
        super().__init__()
        self.__policy_id = None

    @property
    def policy_id(self):
        return self.__policy_id

    @policy_id.setter
    def policy_id(self, policy_id):
        self.__policy_id = policy_id

    def generate_id(self, module_params: dict) -> object:
        return module_params['name']

    def delete(self):
        assert self.policy_id is not None
        return self._svc.delete(self.policy_id)

    def create(self):
        payload = self._requested_config.signature
        return self._svc.create(self.id,
                                datacenters=payload.get('Datacenters'),
                                description=payload['Description'],
                                rules=payload['Rules'])

    def read(self):
        assert self.policy_id is not None
        return self._svc.get(self.policy_id)

    def update(self):
        assert self.policy_id is not None
        payload = self._requested_config.signature

        return self._svc.update(self.policy_id,
                                self.id,
                                datacenters=payload.get('Datacenters'),
                                description=payload['Description'],
                                rules=payload['Rules'])

    def get_requested_config(self, module_params) -> Config:
        config = PolicyConfig.factory_config_from_module_params(module_params)
        return config

    def get_existing_config(self, id):
        return self._svc.get(id)

    def find_existing_id(self) -> str:
        policies = self._svc.list()
        result_ = next((i for i in policies if i["Name"] == self.id_), {})
        if result_:
            return result_['ID']
        else:
            return ""

    def __repr__(self):
        return f"ConsulPolicy Configurable.\n" + \
               f"changes pending {self.changes_pending}\n" + \
        f"display name: {self.display_name}\n" + \
               f"id: {self.id}\n" + \
            f"policy id: {self.policy_id}" + \
               f"exists: {self.exists}\n" + \
        f"existing_config: {self._existing_config}\n" + \
            f"requested_config: {self._requested_config}"


class PolicyBuilder(Builder):
    def __init__(self, module_: AnsibleModule, svc_: ModuleSvc) -> None:
        self.module_ = module_
        self._svc = svc_
        self.reset()

    def reset(self) -> None:
        self._configurable = Policy()

    @property
    def configurable(self) -> Policy:
        configurable = self._configurable
        self.reset()
        return configurable

    def produce_service(self) -> None:
        self._configurable._svc = self._svc

    def produce_id(self) -> None:
        self._configurable.id_ = self._configurable.generate_id(self.module_.params)

    def produce_requested_config(self) -> None:
        self._configurable._requested_config = self._configurable.get_requested_config(self.module_.params)

    def process_existing(self) -> None:
        # The Policy list endpoint does not return a full Policy configuration (Rules are missing)
        # So we have to make one two queries, one against the list endpoint to see if there is a record
        # whose name matches the Policy ID, and a second to grab the full configuration.
        self._configurable.policy_id = self._configurable.find_existing_id()
        if self._configurable.policy_id:
            # use the policy id to get the full Policy configuration
            policy = self._configurable.read()
            self._configurable._existing_config = PolicyConfig.factory_config_from_query(policy)
        else:
            self._configurable._existing_config = PolicyConfig({})


POLICY_ARGUMENT_SPEC = dict(datacenters=dict(default=None, required=False, type='list'),
                            description=dict(default='', required=False, type='str'),
                            name=dict(required=True, type='str', ),
                            namespace=dict(required=False, type='str'),
                            rules=dict(required=True, type='str'),
                            state=dict(type="str", choices=['present', 'absent'], default='present'))

# Combine Consul connection parameters with role specific parameters)
ARGUMENT_SPEC = {**CONSUL_CONNECTION_ARGUMENT_SPEC, **POLICY_ARGUMENT_SPEC}


def init_svc(module_params):
    client = consul.Consul(host=module_params.pop('host'),
                           token=module_params.pop('auth_token'),
                           port=module_params.pop('port'),
                           scheme=module_params.pop('scheme'))
    return PolicySvc(client)


if __name__ == '__main__':
    module = AnsibleModule(ARGUMENT_SPEC)
    svc = init_svc(module.params)
    director = Director()
    builder = PolicyBuilder(module, svc)
    director.builder = builder
    director.build()

    # module.check_mode = True
    result = ModuleRunner(builder.configurable, module).run()
    module.exit_json(changed=result.changed,
                     meta=result.meta,
                     msg="{}".format(result.msg))
