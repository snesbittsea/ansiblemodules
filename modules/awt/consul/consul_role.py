#!/usr/bin/python

# Copyright: (c) 2020, Stephen Nesbitt <steve.nesbitt@aussieswithtails.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# ToDo - policy suboptions name/id are mutually exclusive. one or the other. Needs enforcement.
# ToDo - node_identities require presence of policies stanza. Needs enforcement.
# ToDo - review and test module parameters to ensure accuracy and correct behavior

ANSIBLE_METADATA = {'metadata_version': '1.1', 'status': ['preview'], 'supported_by': 'awt'}

DOCUMENTATION = r'''
---
module: consul_role

short_description: manage Hashicorp Consul Roles

version_added: "2.9"

short_description:
    - C(consul_role) is used to create and maintain Consul ACL roles.

options:
    auth_token: 
        description: 
            - Consul token id. It must ACL read, write, and delete privileges.
        required: true
        type: str
    description:
        default: ''
        description: 
            - A human readable description of the role.
        type: str
    host:
        default: 127.0.0.1
        description: 
            - The address of the Consul host responsible for servicing the request.
        required: false
        type: str
    name:
        description:
            - The name of the role. Restricted to alphanumerics, dashes and underscores.
        type: str
        required: true
    namespace:
        description: 
            - Specifies the namespace to create the role. If not provided in the JSON body, 
            the value of the ns URL query parameter or in the X-Consul-Namespace header will be used. 
            If not provided at all, the namespace will be inherited from the request's ACL token or will 
            default to the default namespace. Added in Consul 1.7.0
        required: false
        type: str
    node_identities:
        description: 
            - The list of node identities that should be applied to the role. Added in Consul 1.8.1.
        required: false
        type: list
        elements: dict
        suboptions:
            name:
                description: 
                    - The name of the node. The name must be no longer than 256 characters, must start and end 
                        with a lowercase alphanumeric character, and can only contain lowercase alphanumeric characters 
                        as well as - and _.
                required: true
                type: str
            datacenter:
                description: 
                    - Specifies the nodes datacenter. This will result in effective policy
                    only being valid in that datacenter.
                required: true
                type: str.
    port:
        default: 8500
        description: 
            - The port the Consul host responsible for servicing the request is listening on.
        required: false
        type: int
    scheme:
        default: http
        description: 
            - The HTTP scheme to use to connect to the Consul server.
        required: false
        type: int
    service_identities:
        description: 
            - The list of service identities that should be applied to the role. Added in Consul 1.5.0.
        required: false
        type: list
        elements: dict
        suboptions:
            datacenters:
                default: []
                description: A list of datacenters in which the role policies will be in effect. If not
                specified defaults to all datacenters
                required: false
                type: list
                elements: str
            name:
                description: The name of the service. The name must be no longer than 256 characters, must start and
                    end with a lowercase alphanumeric character, and can only contain lowercase alphanumeric characters
                     as well as - and _.
                required: true
                type: str
    state:
        description:
            - Whether the role should be present or absent
        type: str
        choices: [ present, absent ]
        default: [ present ]

author:
    - Stephen Nesbitt <steve.nesbitt@aussieswithtails.com>
'''

EXAMPLES = r'''
# Create A Basic Consul Role
- name: Add Consul Role
  consul_role:
    auth_token: "123456"
    name: "test"-role
    state: present
    
# Create a Consul Role with Service Identities
- name: Add Consul Role
  consul_role:
    name: "test-role_with_sids"
    description: "Test Consul Role with Service Identities"
    service_identities: [{name: 'foo', datacenters: ['westcoast', eastcoast'}]
    state: present


# Create a Consul Role with ServiceIdentities.
- name: Add Consul Role With Policies
  consul_role:
    name: "test-role_with_policies"
    description: "Test Consul Role with Service Identities"
    policies: [{'name': 'foo', 'id': '2222'}]
    service_identities: [ 'name': 'this_service', datacenters=['westcoast']
    state: present
'''

# ToDo - review what this should be
RETURN = r'''
# FixMe
original_message:
    description: The original name param that was passed in
    type: str
    returned: always
message:
    description: The output message that the test module generates
    type: str
    returned: always
'''
from enum import Enum
from ansible.module_utils.awt.base import Builder, Config, Director, ModuleRunner, ModuleSvc
from ansible.module_utils.awt.consul import (CONSUL_CONNECTION_ARGUMENT_SPEC,
                                             ConsulSvc,
                                             ConsulConfigurable)
from ansible.module_utils.basic import AnsibleModule
from consul import Consul


class RoleConfig(Config):
    BaseMap = Enum('RoleMap', [('description', 'Description'),
                               ('namespace', 'Namespace'),
                               ('node_identities', 'NodeIdentities'),
                               ('policies', 'Policies'),
                               ('service_identities', 'ServiceIdentities')])
    NodeIdentityMap = Enum('NodeIdentityMap',
                           [('name', 'NodeName'),
                            ('datacenter', 'Datacenter')]
                           )
    PolicyMap = Enum('BaseMap',
                     [('id', 'ID'),
                      ('name', 'Name')]
                     )
    ServiceIdentityMap = Enum('ServiceIdentityMap',
                              [('name', 'ServiceName'),
                               ('datacenters', 'Datacenters')]
                              )

    @classmethod
    def factory_config_from_module_params(cls, module_params: dict) -> Config:
        config_dict = module_params  # We don't want to modify module_params, so we create a copy
        NODEID_KEY = "node_identities"
        POLICY_KEY = "policies"
        SERVICEID_KEY = "service_identities"

        def map_list(items: list, map: Enum) -> list:
            mapped_list = []
            for node in items:
                # ToDo - need unit test to verify that use of a key the Enum does not have defined does nothing rather
                # than throw an error
                translated = {map[key].value: value for (key, value) in node.items() if key in map.__members__ and
                              map[key].value is not None}
                mapped_list.append(translated)
            return mapped_list

        if module_params.get(NODEID_KEY) is not None:
            config_dict[NODEID_KEY] = map_list(module_params[NODEID_KEY], RoleConfig.NodeIdentityMap)

        if module_params.get(POLICY_KEY) is not None:
            config_dict[POLICY_KEY] = map_list(module_params[POLICY_KEY], RoleConfig.PolicyMap)

        if module_params.get(SERVICEID_KEY) is not None:
            config_dict[SERVICEID_KEY] = map_list(module_params[SERVICEID_KEY],
                                                  RoleConfig.ServiceIdentityMap)

        # Not all module_params are configuration settings so do not process those which are not a member of the token
        # map
        config_dict = {RoleConfig.BaseMap[k].value: v for k, v in module_params.items()
                       if v is not None and k in RoleConfig.BaseMap.__members__}
        return RoleConfig(config_dict)

    @classmethod
    def factory_config_from_query(cls, query_result: dict) -> Config:
        #ToDo - unittest
        #ToDo - this is a candidate for a separate function since used
        # for any ACL item that includes Policies(eg tokens)
        # The Consul API query for a Role includes a list of Policy dictionaries.
        # One of the dictionary keys is ID.
        # Since we lookup a Policy by name not id we have no way to know what the id is
        # and its presence will cause the requested vs existing configuration check to always
        # return changed.
        # Therefore we strip it off.
        if not query_result:
            return RoleConfig({})
        else:
            normalized_policies = []
            for policy in query_result.get('Policies'):
                policy.pop('ID', None)
                normalized_policies.append(policy)

            config_dict = {'Description': query_result.get('Description'),
                           'Namespace': query_result.get('Namespace'),
                           'NodeIdentities': query_result.get('NodeIdentities'),
                           'Policies': query_result.get('Policies'),
                           'ServiceIdentities': query_result.get('ServiceIdentities')}
            config_dict = {k: v for k, v in config_dict.items() if v is not None}
            return RoleConfig(config_dict)

    def __init__(self, signature: dict):
        super().__init__(signature)


class RoleSvc(ConsulSvc):
    @property
    def _endpoint(self):
        return self._client.acl.roles

    def __init__(self, client):
        super().__init__(client)

    def create(self, payload: dict):
        """Creates a new Consul policy object.

         :param payload: the name of the policy to be created
         :type payload: payload: dict

         :return: the result of the API call
         :rtype: dict`
        """
        return self._endpoint.create(payload)

    def lookup_by_name(self, name):
        _result = self._endpoint.get_by_name(name)
        if type(_result) is tuple and _result[1] is None:
            return {}
        else:
            return _result

    def list(self):
        # raise Exception("client: {}".format(str(self._svc.__dict__)))
        return self._endpoint.list()

    def update(self, id, payload) -> dict:
        """Ask the Consul API to update a specific policy

         :param id: the policy id
         :type: id: str
         :param payload: the policy name
         :type: policy: dict
         :return: The results generated by the API.
         :rtype: dict"""
        return self._endpoint.update(id,
                                     payload)


class Role(ConsulConfigurable):
    """Encapsulates a Consul Role object.
    """
    def __init__(self):
        super().__init__()

    def generate_id(self, module_params: dict) -> object:
        return module_params['name']

    def delete(self):
        assert self._accessor_id is not None
        return self._svc.delete(self._accessor_id)

    def create(self):
        payload = self._requested_config.signature
        payload['Name'] = self.id_
        return self._svc.create(payload)

    def update(self):
        assert self._accessor_id is not None
        return self._svc.update(self._accessor_id, self._requested_config)

    def get_requested_config(self, module_params) -> Config:
        config = RoleConfig.factory_config_from_module_params(module_params)
        return config

    def get_existing_config(self):
        existing_roles = self._svc.list()
        result_ = next((i for i in existing_roles if i["Name"] == self.id_), {})
        result_.pop('Name', {})
        return RoleConfig.factory_config_from_query(result_)


class RoleBuilder(Builder):
    def __init__(self, module_: AnsibleModule, svc_: ModuleSvc) -> None:
        self.module_ = module_
        self._svc = svc_
        self.reset()

    def reset(self) -> None:
        self._configurable = Role()

    @property
    def configurable(self) -> Role:
        configurable = self._configurable
        self.reset()
        return configurable

    def produce_service(self) -> None:
        self._configurable._svc = self._svc

    def produce_id(self) -> None:
        self._configurable.id_ = self._configurable.generate_id(self.module_.params)

    def produce_requested_config(self) -> None:
        self._configurable._requested_config = self._configurable.get_requested_config(self.module_.params)

    def process_existing(self) -> None:
        existing_roles = self._configurable.svc.list()
        result_ = next((i for i in existing_roles if i["Name"] == self._configurable.id), {})
        self._configurable._existing_config = RoleConfig.factory_config_from_query(result_)
        self._configurable._accessor_id = result_.get('ID', None)


ROLE_ARGUMENT_SPEC = dict(description=dict(default="", type='str', required=False),
                          name=dict(required=True, type="str"),
                          namespace=dict(type='str', required=False),
                          node_identities=dict(type='list', elements='dict', required=False),
                          policies=dict(type='list', elements='dict', required=False),
                          service_identities=dict(type='list', elements='dict', required=False),
                          state=dict(type="str", choices=['present', 'absent'], default='present'))

# Combine Consul connection parameters with role specific parameters)
ARGUMENT_SPEC = {**CONSUL_CONNECTION_ARGUMENT_SPEC, **ROLE_ARGUMENT_SPEC}


def init_svc(module_params):
    client = Consul(host=module_params.pop('host'),
                    token=module_params.pop('auth_token'),
                    port=module_params.pop('port'),
                    scheme=module_params.pop('scheme'))
    return RoleSvc(client)


if __name__ == '__main__':
    module = AnsibleModule(ARGUMENT_SPEC)
    svc = init_svc(module.params)
    director = Director()
    builder = RoleBuilder(module, svc)
    director.builder = builder
    director.build()

    # module.check_mode = True
    result = ModuleRunner(builder.configurable, module).run()
    module.exit_json(changed=result.changed,
                     meta=result.meta,
                     msg="{}".format(result.msg))
