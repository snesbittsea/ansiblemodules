# AWT Ansible Module Utils Architecture Guide

## Statement of Purpose

The architectural goal is to provide a framework that facilitates developing custom Ansible modules in the AWT
development environment. Specific goals are a framework which:

+ minimizes the amount of code that needs to be written in developing a module.
    The basic assumption here is that much of the code/functionality in a
    custom Ansible module is boilerplate and/or identical among modules.

+ provides a set of core abstractions applicable to all Ansible modules.

+ ensures a significant degree of commonality among AWT developed Ansible modules.

## Core Concepts

### Configurable
Configurable is some entity which can Ansible can create if not present,
delete if present, or update if an existing entity's configuration differs
from the desired/requested configuration. Key implications are:

#### Requested Configuration
+ The Configurable must have a means of identifying/generating a requested configuration.
Within the AWT Module Framework, it is assumed that a requested configuration can be
generated from the Ansible module parameters. With respect to module parameters it should
be noted:

1. Not all of the parameters available in module_params are applicable to
the configuration. For example, the "state" parameter is not part of the configuration
but a directive to the module runner. This implies that part of the requested
configuration generation process is the filtering out of non-configuration items.

2. It is *not* assumed that the Ansible module_params are directly mappable to
the requested configuration. The only assumption is that the requested configuration
can be derived from the module_params using the capabilities of the Configurable class.

#### Existing Configuration
+ There must be a a means of querying an existing datastore of
Configurable instances to definitely locate (or not) an existing instance. This in turn
implies that every Configurable has a unique identifier. *Within the AWT Ansible Module
Framework, it is assumed that the key can be generated from the module_params and the capabilities
of the Configurable class.* A good example of this is the Consul Role Configurable.
API manipulation updates/deletes require the use of an AccessorId. This id is not available
until *after* creation of an initial instance and so cannot be used reliably to query the datastore
for an existing existence.

