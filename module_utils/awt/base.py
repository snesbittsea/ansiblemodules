from abc import ABC, abstractmethod
from ansible.module_utils.basic import AnsibleModule


class Config(ABC):
    @property
    def signature(self):
        return self._signature

    def __init__(self, signature: object):
        self._signature = signature

    def __bool__(self):
        return self._signature != {}

    def __eq__(self, other):
        return self.signature == other.signature

    def __repr__(self):
        return f'{self.__class__.__name__}: signature: {self.signature}'

    def __str__(self):
        return str(self.__dict__)


class ModuleSvc(ABC):
    def __init__(self, *args, **kwargs):
        pass

    def __str__(self):
        return str(self.__dict__)

    @abstractmethod
    def create(self, *args, **kwargs):
        pass

    @abstractmethod
    def delete(self, *args, **kwargs):
        pass

    @abstractmethod
    def list(self, *args, **kwargs):
        pass

    @abstractmethod
    def get(self, *args, **kwargs):
        pass

    @abstractmethod
    def update(self, id, spec):
        pass


class Builder(ABC):
    """
    The Builder interface spacifies methods for creating the different parts
    of the Configurable objects.
    """

    @property
    @abstractmethod
    def configurable(self) -> None:
        pass

    @abstractmethod
    def produce_service(self, *args, **kwargs):
        pass

    @abstractmethod
    def produce_id(self) -> None:
        pass

    @abstractmethod
    def produce_requested_config(self) -> None:
        pass

    @abstractmethod
    def process_existing(self) -> None:
        pass


class Configurable(ABC):
    """This is an abstract class representing an entity which can be configured with Ansible.

    :param id: An identifier which uniquely identifies a configurable from the user's perspective. It is used to
        find, update, create and delete a configurable. As an immutable entity, the id
        is not considered part of a configurable's configuration.
    :type id: class `Object`
        """
    @property
    def id(self):
        """Returns the unique id identifying the Configurable.
        """
        return self.id_

    @property
    def display_name(self):
        return f"{self.__class__.__name__} {self.id_}"

    @property
    def exists(self):
        return True if self._existing_config else False

    @property
    def changes_pending(self) -> bool:
        return True if self._existing_config != self._requested_config else False

    @property
    def svc(self) -> ModuleSvc:
        return self._svc

    def __init__(self):
        self._svc = None
        self.id_ = None
        self._existing_config = None
        self._requested_config = None

    # def set_display_name(self):
    #     return f"{self.__class__.__name__}: {self.id_}"

    @abstractmethod
    def create(self):
        pass

    @abstractmethod
    def delete(self):
        pass

    @abstractmethod
    def update(self):
        pass

    @abstractmethod
    def generate_id(self, module_params: dict) -> object:
        pass

    @abstractmethod
    def get_existing_config(self, id: object) -> Config:
        pass

    @abstractmethod
    def get_requested_config(self, module_params: dict) -> Config:
        pass

    # ToDo __repr__
    def __str__(self):
        return str(self.__dict__)


class ModuleRunner(ABC):
    @property
    def _check_mode(self):
        return self._module.check_mode

    @property
    def configurable_id(self):
        return self._configurable.id_

    @property
    def _should_be_present(self):
        if self._state == 'present':
            return True
        else:
            return False

    def __init__(self,
                 configurable: Configurable,
                 module: AnsibleModule):
        self._configurable = configurable
        self._state = module.params.pop('state')
        self._module = module

    def run(self):
        try:
            if self._should_be_present:
                return self._do_ensure_present()
            else:
                return self._do_ensure_absent()
        except Exception as ex:
            self._module.fail_json(msg=ex)

    def _do_ensure_absent(self):
        """Handler for ensuring a configurable object is absent

        :return: result from executing the absence handler
        :rtype: class `ansible.module_utils.awt.ansible.ModuleResult`"""
        result = ModuleResult(changed=False,
                              msg=f"{self._configurable.display_name} is already absent. No changes")
        if self._configurable.exists:
            result.changed = True
            result.msg = f"Removed {self._configurable.display_name}"
            if self._check_mode:
                result.meta = "Check Mode"
            else:
                result.meta = self._configurable.delete()
        return result

    def _do_ensure_present(self):
        """Handler for the present case

        :return: info regards attempt to ensure item absent
         :rtype: class `ansible.module_utils.awt.ansible.ModuleResult`"""

        if self._configurable.exists:
            return self._handle_update()
        else:
            return self._handle_create()

    def _handle_create(self):
        # raise Exception(f"Create {repr(self._configurable)}")
        result = ModuleResult(changed=True,
                              msg=f"Created {self._configurable.display_name}")
        if self._check_mode:
            result.meta = "Check Mode"
            # return result
        else:
            result.meta = self._configurable.create()
            # return result
        return result

    def _handle_update(self):
        result_ = ModuleResult(msg=f"{self._configurable.display_name} exists as specified. No create/update necessary",
                              changed=False)
        if self._configurable.changes_pending:
            # raise Exception(f"\nrc: {self._configurable._requested_config}\nec: {self._configurable._existing_config}" )
            result_.msg = f"{self._configurable.display_name} has pending changes"
            result_.changed = True
            if self._check_mode:
                result_.meta = "Check Mode"
            else:
                result_.meta = self._configurable.update()
        return result_


class QueryResult(ABC):
    """Abstract base class that represents the the result of a api_query

    :param query_result: a set of key-value pairs representing the result of a api_query
    :type: item: dict
    """

    def __init__(self, query_result):
        """Constructor method"""
        self._query_result = query_result

    @property
    def query_result(self):
        """The api_query result object

        :return the result of a api_query
        :rtype dict"""
        return self._query_result

    def output(self):
        return str(self.__dict__)

    def __str__(self):
        return str(self.__dict__)


class ModuleResult(ABC):
    @property
    def changed(self):
        return self._changed

    @changed.setter
    def changed(self, status: bool):
        self._changed = status

    @property
    def meta(self):
        return self._meta

    @meta.setter
    def meta(self, object):
        self._meta = object

    @property
    def msg(self):
        return self._msg

    @msg.setter
    def msg(self, msg):
        self._msg = msg

    def __init__(self, msg, changed=False, meta=None, **kwargs):
        self._changed = changed
        self._msg = msg
        self._meta = meta
        self.kwargs = kwargs

    def __repr__(self):
        return f"{self.__class__.__name__} - changed: {self._changed}\nmsg: {self._msg}\nmeta: {self._meta}\n"

    def __str__(self):
        return f"ModuleRunnerResult: changed: {self._changed}\nmsg: {self._msg}\nmeta: {self._meta}\n"


class Director(object):
    """ Controls the construction process.

    Director has a builder associated with it. Director then
    delegates building of the smaller parts to the builder and
    the assembles them into a completely instantiated object.
    """
    def __init__(self) -> None:
        self._builder = None

    @property
    def builder(self) -> Builder:
        return self._builder

    @builder.setter
    def builder(self, builder: Builder) -> None:
        self._builder = builder

    def build(self) -> None:
        self.builder.produce_id()
        self.builder.produce_service()
        self.builder.produce_requested_config()
        self.builder.process_existing()


