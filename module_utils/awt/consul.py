from abc import ABC, abstractmethod
from enum import Enum
import consul
from ansible.module_utils.awt.base import Config, Configurable, ModuleSvc

CONSUL_CONNECTION_ARGUMENT_SPEC = dict(auth_token=dict(type="str", required=True, no_log=True),
                                       host=dict(default='127.0.0.1', type='str', required=False),
                                       port=dict(default=8500, type='int', required=False),
                                       scheme=dict(type="str", choices=['http', 'https'], default='http'))

class ConsulConfig(Config):
    @staticmethod
    def map_list(items: list, map: Enum) -> list:
        mapped_list = []
        for node in items:
            # ToDo - need unit test to verify that use of a key the Enum does not have defined does nothing rather
            # than throw an error
            translated = {map[key].value: value for (key, value) in node.items() if key in map.__members__ and
                          map[key].value is not None}
            mapped_list.append(translated)
        return mapped_list


class ConsulSvc(ModuleSvc, ABC):
    @property
    @abstractmethod
    def _endpoint(self):
        pass

    def __init__(self, client):
        super().__init__()
        self._client = client

    @abstractmethod
    def create(self, *args, **kwargs):
        pass

    def delete(self, id):
        return self._endpoint.delete(id)

    def list(self):
        return self._endpoint.list()

    def get(self, id):
        result = {}
        try:
            result = self._endpoint.get(id)
            return result
        except consul.ACLPermissionDenied as ex:
            # With Consul 1.8.3 a query to a non-existent token returns a 403 (permission denied) with a
            # value of 'ACL nor found' rather than a 404 (not found error). There is an excepted bug on this.
            if ex == 'ACL not found':
                raise ex
            else:
                return result

    @abstractmethod
    def update(self, id, config):
        pass


# noinspection PyAbstractClass
class ConsulConfigurable(Configurable):
    def __init__(self):
        self._accessor_id = None
        super().__init__()

    def delete(self):
        self._svc.delete(self.id_)

    def get(self):
        return self._svc.get(self.id_)
