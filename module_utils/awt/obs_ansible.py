from abc import ABCMeta, abstractmethod
from ansible.module_utils.basic import AnsibleModule

class QueryResult(metaclass=ABCMeta):
	"""Abstract base class that represents the the result of a api_query

	:param query_result: a set of key-value pairs representing the result of a api_query
	:type: item: dict
	"""

	def __init__(self, query_result):
		"""Constructor method"""
		self._query_result = query_result

	@property
	def query_result(self):
		"""The api_query result object

		:return the result of a api_query
		:rtype dict"""
		return self._query_result

	def output(self):
		return str(self.__dict__)

	def __str__(self):
		return str(self.__dict__)


class ModuleSvc(metaclass=ABCMeta):
	def __init__(self):
		pass

	def __str__(self):
		return str(self.__dict__)

	@abstractmethod
	def create(self, spec):
		pass

	@abstractmethod
	def delete(self, id):
		pass

	@abstractmethod
	def get(self, specification):
		pass

	@abstractmethod
	def list(self, specification):
		pass

	@abstractmethod
	def update(self, id, spec):
		pass


class ModuleItemMgr(metaclass=ABCMeta):
	''''''

	def __init__(self,
				 name,
				 signature,
				 svc,
				 **kwargs):
		pass

	# @classmethod
	# def object_factory(cls, **kwargs):
	# 	return cls(**kwargs)
	@property
	@abstractmethod
	def existing_signature(self):
		"""Get the signature of an existing item

		:return: the signature of an existing item
		:rtype: class `ansible.module_utils.awt.ansible.ItemSignature`"""
		pass

	@property
	def exists(self):
		return self._exists

	@property
	def name(self):
		return self._name

	@property
	def should_exist(self):
		self._should_exist

	@property
	def signature(self):
		self._spec

	@property
	def svc(self):
		return self._svc

	@abstractmethod
	def _find_existing(self):
		pass

	@abstractmethod
	def _init_signature(self, **module_params):
		"""Initializes the signature of the item being manipulated by the module.

		:param **module_params: The set of module parameters passed in as key value pairs
		:type **module_params: Depends on specific module parameter
		:return: An item specification instance.
		:rtype: class:`ansible.module_utils.awt.ansible.AWTAnsibleItemSignature"""
		pass

	@abstractmethod
	def create(self):
		pass

	@abstractmethod
	def delete(self):
		pass

	def update(self):
		pass

	def __str__(self):
		return str(self.__dict__)


class ItemSignature(metaclass=ABCMeta):
	"""

	"""

	def __init__(self):
		pass

	def __str__(self):
		return str(self.__dict__)


class ModuleResult(object):
	@property
	def dict(self):
		return dict(changed=self._changed,
					msg=self._msg,
					meta=self._meta,
					**self.kwargs)

	def __init__(self, msg, changed=False, meta=None, **kwargs):
		self._changed = changed
		self._msg = msg
		self._meta = meta
		self.kwargs = kwargs

class AnsibleAWTModule(AnsibleModule):
	def __init__(self,  **kwargs):
		super().__init__(**kwargs)

class ModuleBase(metaclass=ABCMeta):
	"""Abstract base class encapsulating an Ansible module.

	:param module: An instance of an AnsibleModule
	:type module: class:`ansible.module_utils.basic.AnsibleModule`
	"""

	def __init__(self, module):
		self._module = module
		self._state = module.params.pop("state")
		svc = self._init_svc(**module.params)
		# configuration = self._init_signature(**module.params)
		self._item_mgr = self._init_mgr(svc=svc,
										**module.params)

	@abstractmethod
	def _init_mgr(self, svc, **module_params):
		"""Initializes an item manager.
		
		:param: item_id: the item's unique unique identifier
		:type: item_id: str
		:type: consul_client: `ansible.module_utils.awt.ansible.AWTAnsibleModuleSignature`
		:param: consul_client: the item service instance
		:type: consul_client: `ansible.module_utils.awt.ansible.AWTAnsibleModuleSignature`
		:param: signature: the signature of the ite instance
		:type: signature: `ansible.module_utils.awt.ansible.ItemSignature`
		:param **module_params: The set of module parameters passed in as key value pairs
		:type **module_params: Depends on specific module parameter
		:return: An item specification instance.
		:rtype: class:`ansible.module_utils.awt.ansible.AWTAnsibleItemSignature`"""
		pass

	@property
	def module(self):
		"""Returns the module object
		:return: An Ansible module instance
		:rtype: class:`ansible.module_utils.basic.AnsibleModule`

		return """
		return self._module

	@abstractmethod
	def _init_svc(self, **module_params):
		"""Initializes the service which will be responsible for interfacing
		with any required external service

		:param **module_params: The set of module parameteres passed in as key value pairs
		:type **module_params: Depends on specific module parameter
		:return: A service instance.
		:rtype: ansible.module_utils.awt.ansible.ModuleSvc"""
		pass

	@property
	def module_params(self):
		return self._module_params

	@property
	def spec(self):
		return self._spec()

	@property
	def is_dry_run(self):
		return self._module.check_mode

	@property
	def item_mgr(self):
		return self._item_mgr

	@property
	def object_exists(self):
		return self._item.exists

	@property
	def params(self):
		return self._module.params

	@property
	def should_exist(self):
		"""Checks to see if the module item should exist

		:return: `True` if state is present, `False` otherwise
		:rtype: bool
		"""
		return True if self._state == 'present' else False

	def execute(self):
		"""Execute the module

		:return the result of the processing
		:rtype ModuleResult"""

		if self.should_exist:
			return self._do_ensure_present()
		else:
			# raise Exception("DEBUG: should be absent")
			return self._do_ensure_absent()

	def _do_ensure_absent(self):
		"""Handler for the absent case

		:return: info regards attempt to ensure item absent
		 :rtype: class `ansible.module_utils.awt.ansible.ModuleResult`"""

		if self._item_mgr.existing:
			return self._do_delete_item()
		else:
			return ModuleResult(changed=False,
								msg="Policy {} is absent".format(self.item_mgr.name))

	def _do_ensure_present(self):
		"""Handler for the present case

		:return: info regards attempt to ensure item absent
		 :rtype: class `ansible.module_utils.awt.ansible.ModuleResult`"""
		if not self.item_mgr.existing:
			result = self._do_create_item()
		elif self.item_mgr.signature != self.item_mgr.existing_signature:
			# raise Exception("Update target: {} {}".format(self.item_mgr.configuration,
			# 											  self.item_mgr.existing_signature))
			result = self._do_update_item()
		else:
			result = ModuleResult(changed=False,
								  msg="Target {} is present".format(self.name))

		return result

	def _do_create_item(self):
		"""Handler for managing a state of present without an existing policy

		:return The result of the create operation
		:rtype class `ansible.module_utils.awt.ansible.ModuleResult`
		"""
		if self.is_dry_run:
			return ModuleResult(changed=True,
								  msg="Created item {}".format(self.item_mgr.name),
								  meta=None)
		else:
			return ModuleResult(changed=True,
								  msg="Created item {}".format(self.item_mgr.name),
								  meta=self.item_mgr.create().output())

	def _do_update_item(self):
		"""Handler for updating state=present and a policywith a changed signature.

		:return The result of the create operation
		:rtype class `ansible.module_utils.awt.ansible.ModuleResult`"""

		if self.is_dry_run:
			return ModuleResult(changed=True,
								msg="Updated {}".format(self._item_mgr.name),
								diff="Original: {}, New: {}".format(self._item_mgr.signature,
																	self._item_mgr.existing_signature))
		else:
			return ModuleResult(changed=True,
								  msg="Updated {}".format(self.item_mgr.name),
								  meta=self.item_mgr.update().output(),
								  diff="Original: {}, New: {}".format(self.item_mgr.signature,
																	  self.item_mgr.existing_signature))


	def _do_delete_item(self):
		"""Processes the request to delete the item

		:return The result of the delete operation
		:rtype class `ansible.module_utils.awt.ansible.ModuleResult`"""

		if self.is_dry_run:
			return ModuleResult(changed=True,
								msg="Deleting {}".format(self.item_mgr.name))
		else:
			return ModuleResult(changed=True,
								msg="Deleting {}".format(self.item_mgr.name),
								meta=self.item_mgr.delete().output())
