import pytest
from units.modules.utils import set_module_args
from ansiblemodules.modules.awt.consul.consul_role import (ARGUMENT_SPEC,
                                                           RoleBuilder,
                                                           RoleConfig,
                                                           RoleSvc,
                                                           Role)
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.awt.base import Director
from collections import namedtuple

# ToDo make consistent with test_token.py

ConfigSet = namedtuple('ConfigSet', ['module_spec', 'api_view'])

# MODULE_ARGUMENT_SPEC = dict(description=dict(required=False, type="str"),
#                             name=dict(required=True, type="str"),
#                             host=dict(default='127.0.0.1', type='str', required=False),
#                             namespace=dict(type='str', required=False),
#                             node_identities=dict(type='list', elements='dict', required=False),
#                             policies=dict(type='list', elements='dict', required=False),
#                             port=dict(default=8500, type='int', required=False),
#                             service_identities=dict(type='list', elements='dict', required=False),
#                             scheme=dict(type="str", choices=['http', 'https'], default='http'),
#                             state=dict(type="str", choices=['present', 'absent'], default='present'))

DEFAULT_PAYLOAD = {
    'Description': 'test role description',
    'Name': 'simple_role',
    'Namespace': 'test_namespace',
    'NodeIdentities': [
        {
            "Datacenter": "dev01",
            "NodeName": "TestNodeIdentity1"
        },
        {
            "Datacenter": "dev02",
            "NodeName": "TestNodeIdentity2"
        }
    ],
    'Policies': [
        {
            "ID": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0",
            "Name": "test1"
        }
    ],
    'ServiceIdentities': [
        {
            "Datacenters": ["dev01", "dev02"],
            "ServiceName": "test_service1"
        },
        {
            "Datacenters": ["dev01"],
            "ServiceName": "test_service2"
        }
    ],
}

TEST_ROLE_LIST_QUERY_RESULT = [
    {"ID": "07bc6e01-68c7-fba8-c79e-31bbcc8ba163",
     "Name": "example-role",
     "Description": "Showcases all input parameters",
     "Hash": "uE6EZ5ZEhWLI0GX7/lfoNBgPHCbuoIMDtn7iu6fbNTw=",
     "CreateIndex": 640093, "ModifyIndex": 640093},
    {"ID": "58abe980-1e7d-d50e-6d63-b0ff8e6b8b53",
     "Name": "test_role1",
     "Description": "test role #1",
     "Policies": [{"ID": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0", "Name": "test1"}],
     "Hash": "McbWCA9v73vdNBYv7DAIdl5XcHd2RSYTKdvmTDkCEcY=",
     "CreateIndex": 208134,
     "ModifyIndex": 376026},
    {"ID": "d759adc1-29dd-b385-13b0-651599a9eb99",
     "Name": "test_role2",
     "Description": "test rol2",
     "Hash": "G5S+TAzu5CfAUN0gn0J/VBpzSmR98t09+sGnXlbp9pw=",
     "CreateIndex": 419804,
     "ModifyIndex": 419804}]

COMPLEX_CONFIG_SPEC = ConfigSet(
    {'auth_token': 'auth_token',
     'description': 'test role description',
     'name': 'simple_role',
     'namespace': 'test_namespace',
     'node_identities': [
         {
             "datacenter": "dev01",
             "name": "TestNodeIdentity1"
         },
         {
             "datacenter": "dev02",
             "name": "TestNodeIdentity2"
         }
     ],
     'policies': [
         {
             "id": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0",
             "name": "test1"
         }
     ],
     'service_identities': [
         {
             "datacenters": ["dev01", "dev02"],
             "name": "test_service1"
         },
         {
             "datacenters": ["dev01"],
             "name": "test_service2"
         }
     ],
     },
    {'Description': 'test role description',
     'Namespace': 'test_namespace',
     'NodeIdentities': [
         {
             "Datacenter": "dev01",
             "NodeName": "TestNodeIdentity1"
         },
         {
             "Datacenter": "dev02",
             "NodeName": "TestNodeIdentity2"
         }
     ],
     'Policies': [
         {
             "ID": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0",
             "Name": "test1"
         }
     ],
     'ServiceIdentities': [
         {
             "Datacenters": ["dev01", "dev02"],
             "ServiceName": "test_service1"
         },
         {
             "Datacenters": ["dev01"],
             "ServiceName": "test_service2"
         }
     ],
     })

# ToDo - Add node_identities, service_identities and namespace to query result
EXISTING_ROLE1 = ConfigSet(
    {"Description": "Showcases all input parameters"},

    {"ID": "07bc6e01-68c7-fba8-c79e-31bbcc8ba163",
     "Name": "example-role",
     "Description": "Showcases all input parameters",
     "Hash": "uE6EZ5ZEhWLI0GX7/lfoNBgPHCbuoIMDtn7iu6fbNTw=",
     "CreateIndex": 640093,
     "ModifyIndex": 640093})

EXISTING_ROLE2 = ConfigSet(
    {"description": "test role #1",
     "policies": [{"id": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0", "name": "test1"}],
     },

    {"ID": "58abe980-1e7d-d50e-6d63-b0ff8e6b8b53",
     "Name": "test_role1",
     "Description": "test role #1",
     "Policies": [{"ID": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0", "Name": "test1"}],
     "Hash": "McbWCA9v73vdNBYv7DAIdl5XcHd2RSYTKdvmTDkCEcY=", "CreateIndex": 208134,
     "ModifyIndex": 376026}
)

EXISTING_ROLE3 = ConfigSet(
    {"description": ""},

    {"ID": "8cf4eb66-e871-9190-4b4d-c4396218bf6e",
     "Name": "example-role1",
     "Description": "",
     "Hash": "iNBd1o005kHeIzADEe2AAsd95m3rtnz8RB0P8hcWxH8=", "CreateIndex": 676452,
     "ModifyIndex": 676452})

EXISTING_ROLE4 = ConfigSet(
    {"description": "test role4"},

    {"ID": "d759adc1-29dd-b385-13b0-651599a9eb99",
     "Name": "test_role2",
     "Description": "test role2", "Hash": "G5S+TAzu5CfAUN0gn0J/VBpzSmR98t09+sGnXlbp9pw=",
     "CreateIndex": 419804, "ModifyIndex": 419804}
)

# EXISTING_TEST_ROLES = [EXISTING_ROLE1, EXISTING_ROLE2, EXISTING_ROLE3, EXISTING_ROLE4]

TEST_NEW_ROLE_SPEC = {
    'auth_token': 'auth token',
    'description': 'test role description',
    'name': 'simple_role',
    'namespace': 'test_namespace',
    'node_identities': [
        {
            "datacenter": "dev01",
            "name": "TestNodeIdentity1"
        },
        {
            "datacenter": "dev02",
            "name": "TestNodeIdentity2"
        }
    ],
    'policies': [
        {
            "id": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0",
            "name": "test1"
        }
    ],
    'service_identities': [
        {
            "datacenters": ["dev01", "dev02"],
            "name": "test_service1"
        },
        {
            "datacenters": ["dev01"],
            "name": "test_service2"
        }
    ],
}

TEST_EXISTING_ROLE_SPEC = {'auth_token': 'auth token',
                           'description': EXISTING_ROLE2.module_spec['description'],
                           'name': EXISTING_ROLE2.api_view['Name'],
                           'policies': EXISTING_ROLE2.module_spec['policies']}

TEST_MODIFIED_ROLE_SPEC = {'auth_token': 'auth_token',
                           'description': 'New Description',
                           'name': EXISTING_ROLE2.api_view['Name'],
                           'policies': [{"id": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0",
                                         "name": "test1"}]}


@pytest.fixture()
def token_generator(mocker):
    def build_role(module_spec):
        set_module_args(module_spec)
        module = AnsibleModule(ARGUMENT_SPEC)
        mock_svc = mocker.Mock(RoleSvc, autospec=True)
        mock_svc.list.return_value = TEST_ROLE_LIST_QUERY_RESULT
        director = Director()
        builder = RoleBuilder(module, mock_svc)
        director.builder = builder
        director.build()
        return builder.configurable

    return build_role


def test_module_params_are_successfully_transformed_into_a_roleconfig(token_generator):
    # Given a role
    test_role = token_generator(COMPLEX_CONFIG_SPEC.module_spec)

    # Then a correct requested_config is set
    assert test_role._requested_config.signature == COMPLEX_CONFIG_SPEC.api_view


def test_displayname_is_set(token_generator):
    # Given a role
    test_role = token_generator(COMPLEX_CONFIG_SPEC.module_spec)

    # The the displau name is set
    assert test_role.display_name


def test_exists_returns_false_if_configurable_does_not_exists(token_generator):
    # Given a role which does not exist
    test_role = token_generator(COMPLEX_CONFIG_SPEC.module_spec)

    # Then a call to exists returns false
    assert not test_role.exists


def test_changes_pending_is_true_if_configurable_does_not_exists(token_generator):
    # Given a role which does not exist
    test_role = token_generator(COMPLEX_CONFIG_SPEC.module_spec)

    # Then there are no pending changes
    assert test_role.changes_pending


def test_exists_returns_true_if_configurable_exists(token_generator):
    # Given a role which does exist
    test_role = token_generator(TEST_EXISTING_ROLE_SPEC)

    # Then the role does not exist
    assert test_role.exists


def test_changes_pending_is_false_when_existing_and_requesting_configs_are_the_same(token_generator):
    # Given a role spc which is identical to an existing role...
    test_role = token_generator(TEST_EXISTING_ROLE_SPEC)

    # Then there are no pending changes
    assert not test_role.changes_pending


def test_changes_pending_is_true_when_existing_and_requesting_configs_differ(token_generator):
    # Given a role spc which is identical to an existing role...
    test_role = token_generator(TEST_MODIFIED_ROLE_SPEC)

    # Then there are no pending changes
    assert test_role.changes_pending


def test_create_calls_service_with_expected_payload(token_generator):
    # Given a role which does not exist
    test_role = token_generator(TEST_NEW_ROLE_SPEC)

    # When create is called
    test_role.create()

    # then
    test_role.svc.create.assert_called_once_with(DEFAULT_PAYLOAD)
