import pytest
from ansible.module_utils.awt.base import Config, Configurable, ModuleSvc
from collections import namedtuple

#
# ConfigFactories = namedtuple('ConfigFactories', ['from_module', 'from_query'])
Configs = namedtuple('Configs', ['existing', 'requested'])

TEST_DISPLAY_NAME = 'Test Display Name'
TEST_ID = "test id"
TEST_CONFIGURABLE_NAME = 'Test Display Name'
TEST_EXISTING_CONFIG = {'description': 'test description'}
TEST_NONEXISTING_CONFIG = {}
TEST_REQUESTED_CONFIG = {'description': 'different test description'}


class TestConfigurable(Configurable):
    def _get_existing_config(self) -> Config:
        pass

    def _generate_requested_config(self) -> Config:
        pass



@pytest.fixture()
def display_name():
    return TEST_DISPLAY_NAME


def generate_configurable_mock(existing_config, requested_config, mocker, display_name=None):
    test_id = TEST_ID
    test_module_params = requested_config
    mock_svc = mocker.Mock(ModuleSvc)
    mocker.patch.object(TestConfigurable, '_get_existing_config', return_value=Config(existing_config))
    mocker.patch.object(TestConfigurable, '_generate_requested_config', return_value=Config(requested_config))
    return TestConfigurable(test_id, test_module_params, mock_svc, TestConfigurable, display_name)


@pytest.fixture()
def test_id():
    return TEST_ID


@pytest.fixture()
def mock_svc(mocker):
    return mocker.Mock(ModuleSvc)


@pytest.fixture()
def existing_config():
    return {'description': 'test description'}


@pytest.fixture()
def requested_config():
    return {'description': 'different test description'}


@pytest.fixture
def mock_configurable_display_name_set(mocker):
    return generate_configurable_mock(existing_config, requested_config, mocker, display_name=TEST_CONFIGURABLE_NAME)


@pytest.fixture()
def mock_configurable_with_no_pending_changes(requested_config, mocker):
    return generate_configurable_mock(requested_config, requested_config, mocker)


@pytest.fixture()
def mock_configurable_with_pending_changes(requested_config, existing_config, mocker):
    return generate_configurable_mock(existing_config, requested_config, mocker)


@pytest.fixture()
def mock_nonexistent_configurable(requested_config, nonexistant_config, mocker):
    return generate_configurable_mock({}, requested_config, mocker)


def test_display_name_generated_if_not_provided(test_id,
                                                existing_config,
                                                requested_config,
                                                mock_svc,
                                                mocker):
    mocker.patch.object(TestConfigurable, '_get_existing_config', return_value=Config(existing_config))
    mocker.patch.object(TestConfigurable, '_generate_requested_config', return_value=Config(requested_config))

    # Given a Configurable with no display_name provided
    test_configurable = TestConfigurable(test_id, requested_config, mock_svc, display_name)

    # Then a display name is generated
    assert test_configurable.display_name != TEST_CONFIGURABLE_NAME


def test_display_name_set_to_provided_value(test_id,
                                            existing_config,
                                            requested_config,
                                            mock_svc,
                                            display_name,
                                            mocker):
    mocker.patch.object(TestConfigurable, '_get_existing_config', return_value=Config(existing_config))
    mocker.patch.object(TestConfigurable, '_generate_requested_config', return_value=Config(requested_config))

    # Given a Configurable with a display_name specified
    test_configurable = TestConfigurable(test_id, requested_config, mock_svc, display_name)

    # Then a display name is not generated
    assert test_configurable.display_name == TEST_CONFIGURABLE_NAME


def test_changes_pending_returns_true_when_existing_and_requested_configs_differ(test_id,
                                                                                 mock_svc,
                                                                                 mocker,
                                                                                 existing_config=TEST_EXISTING_CONFIG,
                                                                                 requested_config=TEST_REQUESTED_CONFIG,
                                                                                 display_name=TEST_DISPLAY_NAME):
    mocker.patch.object(TestConfigurable, '_get_existing_config', return_value=Config(existing_config))
    mocker.patch.object(TestConfigurable, '_generate_requested_config', return_value=Config(requested_config))

    # Given a Configurable in which requested config is different from existing config...
    test_configurable = TestConfigurable(test_id, requested_config, mock_svc, display_name)

    # Then changes_pending returns True
    assert test_configurable.changes_pending


def test_changes_pending_returns_false_when_existing_and_requested_configs_do_not_differ(test_id,
                                                                                         mock_svc,
                                                                                         mocker,
                                                                                         existing_config=TEST_EXISTING_CONFIG,
                                                                                         requested_config=TEST_EXISTING_CONFIG,
                                                                                         display_name=TEST_DISPLAY_NAME):
    mocker.patch.object(TestConfigurable, '_get_existing_config', return_value=Config(existing_config))
    mocker.patch.object(TestConfigurable, '_generate_requested_config', return_value=Config(requested_config))

    # Given a Configurable in which requested config is different from existing config...
    test_configurable = TestConfigurable(test_id, requested_config, mock_svc, display_name)

    # Then changes_pending return False
    assert not test_configurable.changes_pending


def test_create_calls_svc_create_with_id_and_requested_config(test_id,
                                                              mock_svc,
                                                              mocker,
                                                              existing_config={},
                                                              requested_config=TEST_EXISTING_CONFIG,
                                                              display_name=TEST_DISPLAY_NAME):
    mocker.patch.object(TestConfigurable, '_get_existing_config', return_value=Config(existing_config))
    mocker.patch.object(TestConfigurable, '_generate_requested_config', return_value=Config(requested_config))

    # Given a Configurable
    test_configurable = TestConfigurable(test_id, requested_config, mock_svc, display_name)

    # When create is called
    test_configurable.create()

    # Then ModuleSvc.create is called with configurable id, and requested config
    mock_svc.create.assert_called_once_with(test_id, test_configurable._requested_config)


def test_delete_calls_svc_delete_with_configurable_id(test_id,
                                                      mock_svc,
                                                      mocker,
                                                      existing_config=TEST_EXISTING_CONFIG,
                                                      requested_config=TEST_EXISTING_CONFIG,
                                                      display_name=TEST_DISPLAY_NAME):
    mocker.patch.object(TestConfigurable, '_get_existing_config', return_value=Config(existing_config))
    mocker.patch.object(TestConfigurable, '_generate_requested_config', return_value=Config(requested_config))

    # Given a Configurable
    test_configurable = TestConfigurable(test_id, requested_config, mock_svc, display_name)

    # When delete is called
    test_configurable.delete()

    # Then ModuleSvc.delete is called with configurable id
    mock_svc.delete.assert_called_once_with(test_id)


def test_update_calls_svc_update_with_id_and_requested_config(test_id,
                                                              mock_svc,
                                                              mocker,
                                                              existing_config=TEST_EXISTING_CONFIG,
                                                              requested_config=TEST_EXISTING_CONFIG,
                                                              display_name=TEST_DISPLAY_NAME):
    mocker.patch.object(TestConfigurable, '_get_existing_config', return_value=Config(existing_config))
    mocker.patch.object(TestConfigurable, '_generate_requested_config',
                        return_value=Config(requested_config))  # Given a Configurable that exists
    # Given a Configurable
    test_configurable = TestConfigurable(test_id, requested_config, mock_svc, display_name)

    # When asked to update...
    test_configurable.update()

    # Then ModuleSvc.update is called with the configurable id and requested configuration
    mock_svc.update.assert_called_once_with(test_id, test_configurable._requested_config)


def test_instantiation_sets_existing_config(test_id,
                                            mock_svc,
                                            mocker,
                                            existing_config=TEST_EXISTING_CONFIG,
                                            requested_config=TEST_REQUESTED_CONFIG,
                                            display_name=TEST_DISPLAY_NAME):
    mocker.patch.object(TestConfigurable, '_get_existing_config', return_value=Config(existing_config))
    mocker.patch.object(TestConfigurable, '_generate_requested_config', return_value=Config(requested_config))

    # Given a Configurable
    test_configurable = TestConfigurable(test_id, requested_config, mock_svc, display_name)

    # then requested_config is set
    assert test_configurable._existing_config is not None


def test_instantiation_sets_requested_config(test_id,
                                             mock_svc,
                                             mocker,
                                             existing_config=TEST_EXISTING_CONFIG,
                                             requested_config=TEST_REQUESTED_CONFIG,
                                             display_name=TEST_DISPLAY_NAME):
    mocker.patch.object(TestConfigurable, '_get_existing_config', return_value=Config(existing_config))
    mocker.patch.object(TestConfigurable, '_generate_requested_config', return_value=Config(requested_config))

    # Given a Configurable
    test_configurable = TestConfigurable(test_id, requested_config, mock_svc, display_name)

    # Then requested_config is set
    assert test_configurable._existing_config is not None
