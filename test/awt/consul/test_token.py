# ToDo - tests for when secret_id is set or unset.
import pytest
from units.modules.utils import set_module_args
from ansiblemodules.modules.awt.consul.consul_token import (ARGUMENT_SPEC,
                                                            TokenBuilder,
                                                            TokenConfig,
                                                            TokenSvc)
from ansible.module_utils.awt.base import Director
from ansible.module_utils.basic import AnsibleModule
from collections import namedtuple

TEST_PARAMETERS = namedtuple('TestParameters', 'module_spec expected')


@pytest.fixture()
def token_generator(mocker):
    def build_role(module_spec,
                   existing_token,
                   argument_spec=ARGUMENT_SPEC):
        set_module_args(module_spec)
        module = AnsibleModule(argument_spec)
        mock_svc = mocker.Mock(TokenSvc, autospec=True)
        mock_svc.get.return_value = existing_token
        mock_svc.list.return_value = TEST_TOKEN_LIST_QUERY_RESULT
        director = Director()
        builder = TokenBuilder(module, mock_svc)
        director.builder = builder
        director.build()
        return builder.configurable

    return build_role


TESTDATA_EXISTING_TOKENSPEC = TEST_PARAMETERS(
    {'accessor_id': '222fd6d3-7d79-4c46-94cd-6ec08f944b22',
     'auth_token': 'test_token'},
    TokenConfig({'Description': '',
                 'Local': False}))

TESTDATA_SIMPLE_TOKENSPEC = TEST_PARAMETERS(
    {'accessor_id': '123',
     'auth_token': 'test_token'},
    TokenConfig({'Description': '',
                 'Local': False}))

TEST_TOKEN_LIST_QUERY_RESULT = [
    {"AccessorID": "222fd6d3-7d79-4c46-94cd-6ec08f944b22",
     "SecretID": "747c498a-a171-4540-b768-cb83fbf7d6b4",
     "Description": "Complex Token",
     "Policies": [
         {
             "ID": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0",
             "Name": "test1"
         },
         {
             "ID": "00000000-0000-0000-0000-000000000001",
             "Name": "global-management"
         }
     ],
     "Roles": [
         {
             "ID": "07bc6e01-68c7-fba8-c79e-31bbcc8ba163",
             "Name": "example-role"
         },
         {
             "ID": "58abe980-1e7d-d50e-6d63-b0ff8e6b8b53",
             "Name": "test_role1"
         }
     ],
     "ServiceIdentities": [
         {
             "ServiceName": "consul"
         }
     ],
     "Local": True,
     "ExpirationTime": "2020-10-15T18:28:40.848323836Z",
     "CreateTime": "2020-10-14T18:28:40.848323836Z",
     "Hash": "Lkgn+dNaUJXFXSp72BYfFUKk0pXwxZKYbLquC3wCdrA=",
     "CreateIndex": 718848,
     "ModifyIndex": 718848
     },
    {
        "AccessorID": "56225b69-35d4-4b74-a80e-23a3551ceee2",
        "Description": "create test01",
        "Policies": [
            {
                "ID": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0",
                "Name": "test1"
            }
        ],
        "Roles": [
            {
                "ID": "58abe980-1e7d-d50e-6d63-b0ff8e6b8b53",
                "Name": "test_role1"
            }
        ],
        "Local": False,
        "CreateTime": "2020-08-16T18:11:15.534950057Z",
        "Hash": "dTSymUqcaR0ssgN2MYVFXw7Ow7Q6ZbPoa/nOG3HEous=",
        "CreateIndex": 408355,
        "ModifyIndex": 714949
    }]

TESTDATA_COMPLEX_TOKEN_CONFIG = TEST_PARAMETERS(
    {'accessor_id': '123',
     'auth_token': 'test_token',
     'description': 'description',
     'expiration_time': '20',
     'local': True,
     'policies': [{'id': 'pid1'}, {'name': 'pname'}],
     'roles': [{'id': 'rid1'}, {'name': 'rname'}],
     'secret_id': 'test_secret_id',
     'service_identities': [
         {'name': 'svcname1', 'datacenters': ['east', 'west']},
         {'name': 'svcname2', 'datacenters': []}]
     },

    TokenConfig({'Description': 'description',
                 'Local': True,
                 'Policies': [{'ID': 'pid1'}, {'Name': 'pname'}],
                 'Roles': [{'ID': 'rid1'}, {'Name': 'rname'}],
                 'SecretID': 'test_secret_id',
                 'ServiceIdentities': [
                     {'ServiceName': 'svcname1', 'Datacenters': ['east', 'west']},
                     {'ServiceName': 'svcname2', 'Datacenters': []}]})
)


@pytest.mark.parametrize("existing_token", [{}, TEST_TOKEN_LIST_QUERY_RESULT[0]])
def test_displayname_is_set(token_generator, existing_token):
    # Given a role
    test_token = token_generator(TESTDATA_COMPLEX_TOKEN_CONFIG.module_spec, existing_token)

    # The the display name is set
    assert test_token.display_name


@pytest.mark.parametrize("module_spec, expected", [TESTDATA_SIMPLE_TOKENSPEC, TESTDATA_COMPLEX_TOKEN_CONFIG])
def test_requested_config_generates_expected_config(module_spec, expected, token_generator):
    # # Given a Token module specification
    # test_module_spec = module_config.input

    # When a Token is created
    test_role = token_generator(module_spec, existing_token={})
    #
    # Then the expected requested config is generated
    assert test_role._requested_config == expected


def test_exists_returns_false_if_configurable_does_not_exists(token_generator):
    # Given a role which does not exist
    existing_token = {}

    # When a token is instantiated
    test_token = token_generator(TESTDATA_COMPLEX_TOKEN_CONFIG.module_spec, existing_token)

    # Then a call to exists returns false
    assert not test_token.exists


def test_changes_pending_is_true_if_configurable_does_not_exists(token_generator):
    # Given a role which does not exist
    existing_token = {}

    # When a Token Configurable is instantiated
    test_token = token_generator(TESTDATA_COMPLEX_TOKEN_CONFIG.module_spec, existing_token)

    # Then there are no pending changes
    assert test_token.changes_pending


def test_exists_returns_true_if_configurable_exists(token_generator):
    # Given a role which does exist
    existing_token = TEST_TOKEN_LIST_QUERY_RESULT[0]

    # When a Token Configurable is instantiated
    test_role = token_generator(TESTDATA_EXISTING_TOKENSPEC.module_spec, existing_token)

    # Then the role does not exist
    assert test_role.exists


def test_create_calls_service_with_expected_payload(token_generator):
    # Given a token which does not exist
    existing_token = {}
    expected_payload = {**TESTDATA_COMPLEX_TOKEN_CONFIG.expected.signature,
                        **{'AccessorID': TESTDATA_COMPLEX_TOKEN_CONFIG.module_spec['accessor_id']}}
    test_token = token_generator(TESTDATA_COMPLEX_TOKEN_CONFIG.module_spec, existing_token)

    # When create is called
    test_token.create()

    # then
    test_token.svc.create.assert_called_once_with(expected_payload)


def test_update_calls_service_with_expected_payload(token_generator):
    # Given a token which does not exist
    existing_token = {}
    expected_payload = {**TESTDATA_COMPLEX_TOKEN_CONFIG.expected.signature,
                        **{'AccessorID': TESTDATA_COMPLEX_TOKEN_CONFIG.module_spec['accessor_id']}}
    test_token = token_generator(TESTDATA_COMPLEX_TOKEN_CONFIG.module_spec, existing_token)

    # When create is called
    test_token.create()

    # then
    test_token.svc.create.assert_called_once_with(expected_payload)


@pytest.mark.skip
def test_factory_config_from_api_returns_expected_config(config_factory,
                                                         existing_config):
    actual_config = config_factory.factory_generate_config_from_existing(existing_config.input)
    assert actual_config == existing_config.expected

# Configurable tests...
# is the id being set?
# Is the display name set
# Is the requested config set as expected?
# If there is no matching existing configurable, does existing return false?
# If there is an existing configurable, does existing return true?
# If there is an existing configurable, do we have a valid existing config?
# When we call create, is the expected payload created?
# When we call update, is the expected payload sent?
