from ansible.module_utils.basic import AnsibleModule
from ansiblemodules.modules.awt.consul.consul_token import *
# from ansible.module_utils.awt.base import Config
from collections import namedtuple

import pytest
from units.modules.utils import set_module_args, exit_json
from ansible.modules.awt.consul.token import argument_spec

@pytest.fixture
def complex_module_params():
    return {'accessor_id': 'test-accessor',
            'auth_token': 'test-token',
            'description': 'create test05c',
    'local': False,
    'policies': [{'name': 'test1'}],
    'roles': [{'id': '58abe980-1e7d-d50e-6d63-b0ff8e6b8b53'},
              {'name': 'test_role2'}]}

@pytest.fixture
def token_query_result():
    return {
        "AccessorID": "47495b14-a42e-5a39-3a02-cc30e136d518",
        "SecretID": "ece5f08b-b9fe-5708-5671-257c80a8e2fb",
        "Description": "test01",
        "Policies": [
            {
                "ID": "7780740a-6f6d-c32b-fb4c-3fff55fc4eb0",
                "Name": "test1"
            }
        ],
        "Roles": [
            {
                "ID": "58abe980-1e7d-d50e-6d63-b0ff8e6b8b53",
                "Name": "test_role1"
            }
        ],
        "Local": False,
        "CreateTime": "2020-08-13T22:27:03.373534111Z",
        "Hash": "h2Uj5y49CaU3DItOZksifKuei53mj/i3jaRUUv6wbLg=",
        "CreateIndex": 378532,
        "ModifyIndex": 378532
    }


@pytest.fixture
def changed_config(basic_module_params):
    changed_parameters = basic_module_params
    changed_parameters['description'] = "description different"
    return Token.ConfigGenerator.factory_config_from_module_params(changed_parameters)


@pytest.fixture()
def matching_expected_config(basic_module_params):
    return TokenConfigFactory.factory_config_from_module_params(basic_module_params)


@pytest.fixture()
def default_parameters_absent_state():
    return dict([
        ('accessor_id', 'test_accessor_id'),
        ('auth_token', 'test_auth_token'),
        ('state', 'absent')])


@pytest.fixture
def basic_module_params():
    return dict([
        ('accessor_id', 'test_accessor_id'),
        ('auth_token', 'test_auth_token'),
        ('state', 'present')])


@pytest.fixture
def module_parameters_with_policies_and_roles():
    module_only_parameters = ['accessor_id', 'auth_token', 'state']
    module_params = {'accessor_id': 'test_accessor_id',
                     'description': 'test description',
                     'auth_token': 'test_auth_token',
                     'local': False,
                     'policies': [{'id': 'tpolicy_id', 'name': 'tpolicy_name'}],
                     'roles': [{'id': 'trole_id', 'name': 'trole_name'}],
                     'state': 'present'}
    Params = namedtuple('Params', ['module_params', 'token_params'])
    token_params = {name: module_params[name] for (name, value)  in module_params.items() if name not in module_only_parameters }
    return Params(module_params, token_params)

@pytest.fixture
def default_config_state_absent(default_parameters_absent_state, mocker):
    # token_svc = TokenSvc('127.0.0.1', '1234')
    set_module_args(default_parameters_absent_state)
    module = AnsibleModule(argument_spec,
                           supports_check_mode=True)
    mock_token_svc = mocker.MagicMock(TokenSvc)
    # config_factory = TokenConfigFactory()
    # config_factory = TokenConfigFactory

    # noinspection PyTypeChecker
    token = Token('accessor_id', module.params, mock_token_svc)
    return token, module


#
@pytest.fixture
def basic_config(basic_module_params, mocker):
    set_module_args(basic_module_params)
    module = AnsibleModule(argument_spec,
                           supports_check_mode=True)
    mock_token_svc = mocker.patch.object(TokenSvc, 'get', return_value={})
    config_factory = TokenConfigFactory
    token = Token('accessor_id', module.params, mock_token_svc, config_factory)
    return token, module

@pytest.fixture
def complex_config(complex_module_params, mocker):
    set_module_args(complex_module_params)
    module = AnsibleModule(argument_spec,
                           supports_check_mode=True)
    mock_token_svc = mocker.patch.object(TokenSvc, 'get', return_value={})
    config_factory = TokenConfigFactory
    token = Token('accessor_id', module.params, mock_token_svc, config_factory)
    return token, module


#
#
def test_create_not_called_for_new_configurable_when_check_mode_is_true(basic_config, changed_config, mocker):
    (token, module) = basic_config
    module.check_mode = True
    mock_token_create = mocker.patch.object(Token, 'create')

    ModuleRunner(token, module).run()
    mock_token_create.assert_not_called()


def test_create_called_for_new_configurable_when_check_mode_is_false(basic_config, mocker):
    (token, module) = basic_config
    module.check_mode = False

    mock_token_create = mocker.patch.object(Token, 'create')
    ModuleRunner(token, module).run()
    mock_token_create.assert_called_once()


def test_update_not_called_if_check_mode_true_and_no_configuration_change(basic_config, matching_expected_config,
                                                                          mocker):
    (token, module) = basic_config
    module.check_mode = True
    mock_token_create = mocker.patch.object(Token, 'update')

    ModuleRunner(token, module).run()
    mock_token_create.assert_not_called()


def test_update_not_called_if_check_mode_false_and_no_configuration_change(basic_config, matching_expected_config,
                                                                           mocker):
    (token, module) = basic_config
    module.check_mode = True
    mock_token_update = mocker.patch.object(Token, 'update')

    ModuleRunner(token, module).run()
    mock_token_update.assert_not_called()


def test_update_not_called_if_check_mode_true_and_configurations_differ(basic_config, changed_config, mocker):
    token, module = basic_config
    module.check_mode = False  # ToDo - Parameterize
    mock_token_update = mocker.patch.object(Token, 'update')
    mocker.patch.object(Token, 'existing_config', return_value=Config({'test:': 'existing'}))
    mocker.patch.object(Token, 'requested_config', return_value=Config({'test:': 'desired'}))
    ModuleRunner(token, module).run()
    mock_token_update.assert_called()


def test_update_called_if_check_mode_false_and_configurations_differ(basic_config, changed_config, mocker):
    token, module = basic_config
    module.check_mode = False
    mock_token_update = mocker.patch.object(Token, 'update')
    mocker.patch.object(Token, 'existing_config', return_value=Config({'test:': 'existing'}))
    mocker.patch.object(Token, 'existing_config', return_value=Config({'test:': 'desired'}))
    ModuleRunner(token, module).run()
    mock_token_update.assert_called_once()


def test_delete_not_called_if_token_exists_and_check_mode_true(default_config_state_absent, mocker):
    (token, module) = default_config_state_absent
    module.check_mode = True
    mock_token_delete = mocker.patch.object(Token, 'delete')
    mocker.patch.object(Token, 'lookup', return_value=token.desired_config)

    ModuleRunner(token, module).run()
    mock_token_delete.assert_not_called()


def test_delete_called_if_token_exists_and_check_mode_false(default_config_state_absent, mocker):
    (token, module) = default_config_state_absent
    module.check_mode = False
    mocker.patch.object(Token, 'exists', return_value=True)
    mock_token_delete = mocker.patch.object(Token, 'delete')
    mocker.patch.object(Token, 'lookup', return_value=token.desired_config)

    ModuleRunner(token, module).run()
    mock_token_delete.assert_called()


def test_existing_config_correctly_generated_from_module_params(module_parameters_with_policies_and_roles):
    token_params = module_parameters_with_policies_and_roles.token_params
    module_params = module_parameters_with_policies_and_roles.module_params

    actual_config = TokenConfigFactory.factory_config_from_module_params(module_params)
    assert actual_config == Config(token_params)


def test_mapping_from_config_to_api(complex_config, mocker):
    # token, module = complex_config
    # module.check_mode = False
    # mock_token_update = mocker.patch.object(Token, 'update')
    # mocker.patch.object(Token, 'existing_config', return_value=Config({'test:': 'existing'}))
    # mocker.patch.object(Token, 'existing_config', return_value=Config({'test:': 'desired'}))
    # ModuleRunner(token, module).run()
    expected =  {'Description': 'create test05c',
                 'Local': False,
                 'Policies': [{'Name': 'test1'}],
                 'Roles': [{'ID': '58abe980-1e7d-d50e-6d63-b0ff8e6b8b53'}, {'Name': 'test_role2'}]}
    token = complex_config[0]
    actual = token.create()
    # actual = TokenConfigFactory.config_to_payload(Config(expected))
    assert actual == expected
    assert False
