import pytest
from units.modules.utils import set_module_args
from ansiblemodules.modules.awt.consul.consul_policy import (ARGUMENT_SPEC,
                                                             PolicyBuilder,
                                                             PolicyConfig,
                                                             PolicySvc)
from ansible.module_utils.awt.base import Director
from ansible.module_utils.basic import AnsibleModule
from collections import namedtuple


# ToDo
# test when accessor_id is not set
TEST_PARAMETERS = namedtuple('TestParameters', 'module_spec config')

TESTDATA_SIMPLE_POLICY = TEST_PARAMETERS(
    {'auth_token': 'test_auth_token',
     'name': 'test simple policy config',
     'rules': """
              acl = "write"
              agent_prefix "" {
                  policy = "write"
              }"""
     },
    PolicyConfig(
        {'Description': '',
         'Rules': """
              acl = "write"
              agent_prefix "" {
                  policy = "write"
              }"""}))

TESTDATA_COMPLEX_POLICY = TEST_PARAMETERS(
    {'auth_token': 'test_auth_token',
     'datacenters': ['dc1', 'dc2'],
     'description': 'complex policy config',
     'name': 'test complex policy config',
     'namespace': 'test_namespace',
     'rules': """
              acl = "write"
              agent_prefix "" {
                  policy = "write"
              }"""
     },
    PolicyConfig(
        {'Datacenters': ['dc1', 'dc2'],
         'Description': 'complex policy config',
         'Namespace': 'test_namespace',
         'Rules': """
              acl = "write"
              agent_prefix "" {
                  policy = "write"
              }"""}))

TESTDATA_EXISTING_POLICIES = [{"ID": "00000000-0000-0000-0000-000000000001",
                               "Name": "test_policy1",
                               # "Datacenters": [],
                               "Description": "Test Policy 1",
                               "Rules": """
                               acl = "write"
                               agent_prefix "" {
                                    policy = "write"
                                }""",
                               "Hash": "swIQt6up+s0cV4kePfJ2aRdKCLaQyykF4Hl1Nfdeumk=",
                               "CreateIndex": 4,
                               "ModifyIndex": 4}]

TESTDATA_EXISTING_POLICY = TEST_PARAMETERS(
    {'auth_token': 'test_auth_token',
     'name': TESTDATA_EXISTING_POLICIES[0]['Name'],
     'datacenters': TESTDATA_EXISTING_POLICIES[0].get('Datacenters'),
     'description': TESTDATA_EXISTING_POLICIES[0]['Description'],
     'rules': TESTDATA_EXISTING_POLICIES[0]['Rules']},
    PolicyConfig({'Datacenters': TESTDATA_EXISTING_POLICIES[0].get('Datacenters'),
                  'Description': TESTDATA_EXISTING_POLICIES[0]['Description'],
                  'Name': TESTDATA_EXISTING_POLICIES[0]['Name'],
                  'Rules': TESTDATA_EXISTING_POLICIES[0]['Rules']}))

TESTDATA_UPDATED_POLICY = {'auth_token': 'test_auth_token',
                                'name': TESTDATA_EXISTING_POLICIES[0]['Name'],
                                'description': 'New description',
                                'rules': TESTDATA_EXISTING_POLICIES[0]['Rules']}

TESTDATA_EXISTING_POLICY1 = TEST_PARAMETERS(
    {'auth_token': 'test_auth_token',
     'name': TESTDATA_EXISTING_POLICIES[0]['Name'],
     # 'datacenters': [],
     'description': TESTDATA_EXISTING_POLICIES[0]['Description'],
     'rules': 'rule'},
    PolicyConfig({'Description': TESTDATA_EXISTING_POLICIES[0]['Description'],
                  # 'Datacenters': [],
                  'Name': TESTDATA_EXISTING_POLICIES[0]['Name'],
                  'Rules': 'rule'}))


@pytest.fixture()
def policy_generator(mocker):
    def build_role(module_spec,
                   existing_policy,
                   argument_spec=ARGUMENT_SPEC):
        set_module_args(module_spec)
        module = AnsibleModule(argument_spec)
        mock_svc = mocker.Mock(PolicySvc, autospec=True)
        mock_svc.get.return_value = existing_policy
        mock_svc.list.return_value = TESTDATA_EXISTING_POLICIES
        director = Director()
        builder = PolicyBuilder(module, mock_svc)
        director.builder = builder
        director.build()
        return builder.configurable

    return build_role


def test_id_set_to_policy_name(policy_generator):
    expected_id = TESTDATA_SIMPLE_POLICY.module_spec['name']
    # When a new Policy is instantiated
    test_policy = policy_generator(TESTDATA_SIMPLE_POLICY.module_spec, {})

    # Then the id is set to the Policy's name
    assert test_policy.id == expected_id


@pytest.mark.parametrize("existing_policy", [{}, TESTDATA_EXISTING_POLICIES[0]])
def test_displayname_is_set(policy_generator, existing_policy):
    # Given a Policy
    test_policy = policy_generator(TESTDATA_EXISTING_POLICY.module_spec, existing_policy)

    # Then the display name is set
    assert test_policy.display_name


@pytest.mark.parametrize("module_spec, config", [TESTDATA_SIMPLE_POLICY, TESTDATA_COMPLEX_POLICY])
def test_requested_config_successfully_generated_from_module_params(module_spec, config, policy_generator):
    expected_config = config
    # When a Policy is created
    test_policy = policy_generator(module_spec, existing_policy={})

    # Then the expected requested config is generated
    assert expected_config == test_policy._requested_config


def test_exists_returns_false_if_no_matching_configurable_found(policy_generator):
    # Given a Policy which does not exist
    existing_policy = {}

    # When a token is instantiated
    test_policy = policy_generator(TESTDATA_COMPLEX_POLICY.module_spec, existing_policy)

    # Then a call to exists returns false
    assert not test_policy.exists


def test_exists_returns_true_if_matching_configurable_found(policy_generator):
    # Given a Policy which does exist
    existing_policy = TESTDATA_EXISTING_POLICIES[0]

    # When a Policy Configurable is instantiated
    test_policy = policy_generator(TESTDATA_EXISTING_POLICY.module_spec, existing_policy)

    # Then the role does not exist
    assert test_policy.exists


def test_changes_pending_is_true_if_no_matching_configurable_exists(policy_generator):
    # Given a Policy which does not exist
    existing_policy = {}

    # When a Policy Configurable is instantiated
    test_policy = policy_generator(TESTDATA_COMPLEX_POLICY.module_spec, existing_policy)

    # Then there are no pending changes
    assert test_policy.changes_pending


def test_changes_pending_is_true_if_configurable_exists_and_existing_config_different_than_requested_config(policy_generator):
    # Given a Policy which exists
    existing_policy = TESTDATA_EXISTING_POLICIES[0]

    # When a Policy Configurable is instantiated
    test_policy = policy_generator(TESTDATA_UPDATED_POLICY, existing_policy)

    # Then there are pending changes
    assert test_policy.changes_pending


def test_changes_pending_is_false_if_configurable_exists_and_existing_and_requested_configs_match(policy_generator):
    # Given a Policy which exists
    existing_policy = TESTDATA_EXISTING_POLICIES[0]

    # When a Policy Configurable is instantiated
    test_policy = policy_generator(TESTDATA_EXISTING_POLICY.module_spec, existing_policy)

    # Then there are no pending changes
    assert not test_policy.changes_pending


@pytest.mark.parametrize("spec", [TESTDATA_SIMPLE_POLICY, TESTDATA_COMPLEX_POLICY])
def test_create_calls_service_with_expected_payload(spec, policy_generator):
    # Given a token which does not exist
    existing_policy = {}

    # When a Policy is instantiated
    test_policy = policy_generator(spec.module_spec, existing_policy)

    # When create is called
    test_policy.create()

    # then
    test_policy.svc.create.assert_called_once()


def test_update_calls_service_with_expected_payload(policy_generator):
    # Given an existing Policy with an existing token
    existing_policy = TESTDATA_EXISTING_POLICIES[0]
    test_policy = policy_generator(TESTDATA_EXISTING_POLICY1.module_spec, existing_policy)

    # When update is called
    test_policy.update()

    # then
    test_policy.svc.update.assert_called_once()

    #
    # def test_update_calls_service_with_expected_payload(policy_generator):
    #     # Given a token which does not exist
    #     existing_policy = {}
    #     expected_payload = {**TESTDATA_COMPLEX_POLICY.expected.signature,
    #                         **{'AccessorID': TESTDATA_COMPLEX_POLICY.module_spec['accessor_id']}}
    #     test_policy = policy_generator(TESTDATA_COMPLEX_POLICY.module_spec, existing_policy)
    #
    #     # When create is called
    #     test_policy.create()
    #
    #     # then
    #     test_policy.svc.create.assert_called_once_with(expected_payload)
    #
    #
    # @pytest.mark.skip
    # def test_factory_config_from_api_returns_expected_config(config_factory,
    #                                                          existing_config):
    #     actual_config = config_factory.factory_generate_config_from_existing(existing_config.input)
    #     assert actual_config == existing_config.expected

    # Configurable tests...
    # is the id being set?
    # Is the display name set
    # Is the requested config set as expected?
    # If there is no matching existing configurable, does existing return false?
    # If there is an existing configurable, does existing return true?
    # If there is an existing configurable, do we have a valid existing config?
    # When we call create, is the expected payload created?
    # When we call update, is the expected payload sent?
