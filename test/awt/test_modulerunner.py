# ToDo Refactor with respect to naming and to DRYness
from collections import namedtuple

import pytest
from ansible.module_utils.awt.base import Configurable, ModuleRunner
from ansible.module_utils.basic import AnsibleModule
from units.modules.utils import set_module_args

ParamInfo = namedtuple('ParamInfo', ['arg_spec', 'input_spec'])


def generate_parameter_setup(state):
    arg_spec = dict(id=dict(type="str", required=True),
                    description=dict(default="", type='str', required=False),
                    name=dict(type='str', required=True),
                    state=dict(type="str", choices=['present', 'absent'], default='present'))
    call_spec = {'id': 'test-id',
                 'description': 'test-description',
                 'name': 'test-name',
                 'state': state}
    return ParamInfo(arg_spec=arg_spec, input_spec=call_spec)


@pytest.fixture
def configuration_absent():
    return generate_parameter_setup(state="absent")


@pytest.fixture
def configuration_present():
    return generate_parameter_setup(state="present")


@pytest.fixture
def module_absent_check_mode_true(configuration_absent):
    set_module_args(configuration_absent.input_spec)
    test_module = AnsibleModule(configuration_absent.arg_spec)
    test_module.check_mode = True
    return test_module


@pytest.fixture
def module_absent_check_mode_false(configuration_absent):
    set_module_args(configuration_absent.input_spec)
    test_module = AnsibleModule(configuration_absent.arg_spec)
    test_module.check_mode = False
    return test_module


@pytest.fixture
def module_present_check_mode_true(configuration_present):
    set_module_args(configuration_present.input_spec)
    test_module = AnsibleModule(configuration_present.arg_spec)
    test_module.check_mode = True
    return test_module


@pytest.fixture
def module_present_check_mode_false(configuration_present):
    set_module_args(configuration_present.input_spec)
    test_module = AnsibleModule(configuration_present.arg_spec)
    test_module.check_mode = False
    return test_module


@pytest.fixture()
def mock_existing_configurable(mocker):
    mock_configurable = mocker.Mock(Configurable, autospec=True)
    type(mock_configurable).display_name = mocker.PropertyMock(return_value='Mock Configurable')
    type(mock_configurable).exists = mocker.PropertyMock(return_value=True)

    return mock_configurable


@pytest.fixture()
def mock_absent_configurable(mocker):
    mock_configurable = mocker.Mock(Configurable, autospec=True)
    type(mock_configurable).exists = mocker.PropertyMock(return_value=False)
    type(mock_configurable).display_name = mocker.PropertyMock(return_value="Mock Configurable")

    return mock_configurable


def test_create_called_when_state_is_present_and_check_mode_is_false_and_configurable_does_not_exist(
        module_present_check_mode_false,
        mock_absent_configurable):
    test_module = module_present_check_mode_false
    mock_config = mock_absent_configurable
    ModuleRunner(mock_config, test_module).run()
    assert mock_config.create.call_count == 1


def test_create_not_called_when_state_is_present_and_check_mode_is_true(module_present_check_mode_true,
                                                                        mocker):
    test_module = module_present_check_mode_true
    mock_configurable = mocker.Mock(Configurable)
    ModuleRunner(mock_configurable, test_module).run()
    assert mock_configurable.create.call_count == 0


def test_create_not_called_when_state_is_present_and_check_mode_is_false_and_configurable_exists(
        module_present_check_mode_false,
        mock_existing_configurable):
    test_module = module_present_check_mode_false
    mock_configurable = mock_existing_configurable

    ModuleRunner(mock_configurable, test_module).run()
    assert mock_configurable.create.call_count == 0


def test_update_called_when_state_is_present_and_changes_requested_and_check_mode_is_false(
        mock_existing_configurable,
        mocker,
        module_present_check_mode_false):
    test_module = module_present_check_mode_false
    mock_configurable = mock_existing_configurable
    type(mock_configurable).changes_pending = mocker.PropertyMock(return_value=True)
    ModuleRunner(mock_configurable, test_module).run()
    assert mock_configurable.update.call_count == 1


def test_update_not_called_when_state_is_present_and_changes_requested_and_check_mode_is_true(
        mock_existing_configurable,
        mocker,
        module_present_check_mode_true):
    test_module = module_present_check_mode_true
    mock_configurable = mock_existing_configurable
    type(mock_configurable).changes_pending = mocker.PropertyMock(return_value=True)
    ModuleRunner(mock_configurable, test_module).run()
    assert mock_configurable.update.call_count == 0


def test_update_not_called_when_state_is_present_with_no_configuration_changes_and_check_mode_is_false(
        mock_existing_configurable,
        mocker,
        module_present_check_mode_false):
    test_module = module_present_check_mode_false
    mock_configurable = mock_existing_configurable
    type(mock_configurable).changes_pending = mocker.PropertyMock(return_value=False)
    ModuleRunner(mock_configurable, test_module).run()
    assert mock_configurable.update.call_count == 0


def test_delete_called_when_state_absent_and_check_mode_is_false_and_configurable_exists(
        mock_existing_configurable,
        module_absent_check_mode_false):
    test_module = module_absent_check_mode_false
    mock_configurable = mock_existing_configurable
    ModuleRunner(mock_configurable, test_module).run()
    assert mock_configurable.delete.call_count == 1


def test_delete_not_called_when_state_absent_and_check_mode_is_true_and_configurable_exists(
        mock_existing_configurable,
        module_absent_check_mode_true):
    test_module = module_absent_check_mode_true
    mock_configurable = mock_existing_configurable
    ModuleRunner(mock_configurable, test_module).run()
    assert mock_configurable.delete.call_count == 0


# @pytest.mark.skip(reason="not implemented")
def test_delete_not_called_when_state_absent_and_check_mode_is_true_and_configurable_does_not_exist(
        mock_absent_configurable,
        module_absent_check_mode_true):
    test_module = module_absent_check_mode_true
    mock_configurable = mock_absent_configurable
    ModuleRunner(mock_configurable, test_module).run()
    assert mock_configurable.delete.call_count == 0
